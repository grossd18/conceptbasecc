
\chapter{Programming Interface for a C Client: libCB}
\label{cha:c-client}


This chapter describes the programming interface for ConceptBase.
The programming interface consists of a number of data structures
and C functions which are defined in the header file {\ttfamily CBinterface.h}.
Make sure that this header file is included in each of source files
that use functions of libCB.
The data structures are explained in section \ref{sec:data-structures}.
The C library {\tt libCB} contains all functions described in section
\ref{sec:Functions}.

The libraries can be found in the following directories:
\begin{description}
\item[Solaris/SPARC:] The directory \verb+$CB_HOME/sun4/lib+ contains the libraries
for static linking of your application.
\item[Solaris/PC:] The directory \verb+$CB_HOME/i86pc/lib+ contains the libraries
for static linking of your application.
\item[Linux:] The directory \verb+$CB_HOME/linux/lib+ contains the libraries
for static linking of your application.
\item[Windows:] The directory \verb+$CB_HOME/windows/lib+ contains the dynamic libraries
for dynamic linking of your application.
\end{description}

There are currently no plans to build dynamic libraries for the Unix-based
platforms.

The directories \verb+$CB_HOME/examples/Clients/LogClient+ and \\
\verb+$CB_HOME/examples/Clients/C_Client+ contain example
programs, which uses the programming interface {\tt libCB} to communicate
with the ConceptBase server. The {\ttfamily LogClient} program is explained in
appendix \ref{cha:logclient}. Information on you how to compile and link
your source code with the ConceptBase libraries can be found either in
the directories of the example clients or in section \ref{sec:compileandlink}.

\section{Data Structures}
\label{sec:data-structures}

This section describes the data structures used by the API, in particular
structures which are passed to and returned by the interface procedures.

The following C-types are defined in the file {\tt CBinterface.h}
which is located in the directory \verb+$CB_HOME/include+.


\subsection{Completion}
\label{completion}

\begin{verbatim}
typedef enum {CB_OK=0, CB_ERROR, CB_NOT_HANDLED,
              CB_TIMEOUT, CB_CONN_BROKEN} Completion;
\end{verbatim}

The different return values have to be interpreted as follows:

\begin{description}
\item[CB\_OK] the message has been handled successfully.

\item[CB\_ERROR] an error occurred during the execution of the message; the
  ConceptBase server stores some error reports for you on your message
  queue which may be read calling {\tt em = get\_errormessages()} (see below).

\item[CB\_NOTIFICATION] indicates that the message is a notification message.
Notification messages are sent by the server if the client has requested
notification on updates on certain views.

\item[CB\_NOT\_HANDLED] the server was not able to manage your message at all.
  This may be due to an invalid format of input parameters (e.g. wrong
  Telos syntax) or missing parameters.

\item[CB\_TIMEOUT] the message has been sent successfully to the server,
  but there has been no answer from the server after a specific amount
  of time (depends on the type of message sent). This may be due to
  the number of clients which are active or due to the kind of message
  you sent (some queries may last longer than others). The client is
  responsible for the correct handling of answers returned after
  CB\_TIMEOUT occured.

\item[CB\_CONN\_BROKEN] the sending of the last message failed (the
  connection to the server is no longer accessible). Again, the client
  is responsible to handle this return value (e.g. stopping the
  client).
\end{description}

\subsection{Answer}

\begin{verbatim}
struct answer { char *sender;
                 Completion completion;
                 char *return_data;
               };
typedef struct answer Answer;
\end{verbatim}

The {\ttfamily Answer} structure is returned by most library functions.
The first field {\ttfamily sender} contains the name of the sender
as it is maintained by the ConceptBase server. The second one specifies the
status of the message processing (see section \ref{completion})
while the third one contains return values of the message called.
\subsection{Server}

\begin{verbatim}
struct server { char *serverName;
                 char *client;
                 int connected_to_CB_server;
                 SOCKET socket;
               };
typedef struct server Server;
\end{verbatim}

This structure is allocated and filled by the {\tt
  connect\_CB\_server()} call and used as an anchor by all the other
routines to get the right server. The field
{\ttfamily connected\_to\_CB\_server} should usually be true,
as it indicates that the client is connected to the server (or not).
The {\ttfamily socket} field represents the socket which is used
for the communication with the CB server and should be used only
internally.

\subsection{Clients}

\begin{verbatim}
struct clients { char *client;
                 char *toolclass;
                 char *username;
                 struct clients *next;
                  };
typedef struct clients Clients;
\end{verbatim}

This structure represents a simply linked list of clients.
A pointer to this structure is returned as result of the {\tt report\_clients} call.

\subsection{Error\_Messsages}
\label{errormessages}

\begin{verbatim}
struct errormessages { char *errormessage;
                       struct errormessages *next;
                     };
typedef struct errormessages Error_Messages;
\end{verbatim}

List of error messages given by the ConceptBase server every time a
communication event can not be processed correctly. This list may be
obtained calling {\tt get\_errormessages()}.

\section{Functions}
\label{sec:Functions}


\subsection{connect\_CB\_server}

\begin{verbatim}
int connect_CB_server(int      portnr,
                      char    *hostname,
                      char    *clientname,
                      char    *username,
                      Server **server)
\end{verbatim}

\desc{Sets up a connection to a given ConceptBase server. This routine has to be
called once before calling one of the following routines.}
\inputpar{\begin{description}
\item[portnumber] number of the port of the server (this port number is
  unique per server as may be defined at the server's start up time).
\item[hostname] name of the machine on which you started the server
\item[clientname] name of the client to be connected (e.g. {\it TelosEditor\/})
\item[username] name of the user who started the client
\item[server] pointerpointer to a struct server; on a succesfull
  connection the structure will be allocated and filled
\end{description}}
\result{\begin{description}
\item[0] Connection established
\item[-1] There is no such server (probably wrong portnumber and/or host)
\item[$>$0] a completion value (see section \ref{completion})
\end{description}}

\subsection{disconnect\_CB\_server}
\begin{verbatim}
int disconnect_CB_server(Server *server)
\end{verbatim}

\desc{Closes a previous connection to a ConceptBase server. This procedure has to
be called every time a client is stopped (but usually the CBserver is not affected
by clients that crash or do not disconnect correctly).}

\inputpar{\begin{description}
\item[sever] pointer to the structure discribing the current ConceptBase server
\end{description}}

\result{\begin{description}
\item[0] Connection correctly terminated
\item[-1] error, not connected
\item[$>$0] a Completion value
\end{description}}


\subsection{tellCB}
\begin{verbatim}
Answer* tellCB(Server *server, char *objects)
\end{verbatim}

\desc{Inserts a set of objects into the ConceptBase server. This function has been
renamed from previous releases as {\ttfamily tell} is a operating system function
on some systems.}

\inputpar{\begin{description}
\item[server] pointer to the structure discribing the actual server
\item[objects] pointer to a list of objects, which should be inserted into
  the knowledge base. This should be a normal NULL-terminated C-string.
\end{description}}


\result{An answer struct where {\ttfamily return\_data} is either {\ttfamily yes} or {\ttfamily no}
and where the completion value indicates the result of the operation:
\begin{description}
\item[CB\_OK] operation sucessfull
\item[CB\_ERROR] There was an error while
  inserting, get the errormessages by calling {\tt get\_errormessages()}
\item[other] see the description in section \ref{completion}
\end{description}}

\subsection{untell}
\begin{verbatim}
Answer* untell(Server *server, char *objects)
\end{verbatim}

\desc{Removes a list of objects from the knowledge base. Note the specific
semantics of the untell method as described in chapter \ref{serverinterface}
 of this Manual.}

\inputpar{\begin{description}
\item[server] pointer to the structure discribing the actual server
\item[objects] pointer to a list of objects, which should be deleted.
This should be a normal NULL-terminated C-string.
\end{description}}

\result{An answer struct where {\ttfamily return\_data} is either {\ttfamily yes} or {\ttfamily no}
and where the completion value indicates the result of the operation:
\begin{description}
\item[CB\_OK] operation sucessfull
\item[CB\_ERROR] There was an error while removing, get the errormessages
  calling {\tt get\_errormessages()}
\item[other] see the description in section \ref{completion}
\end{description}}


\subsection{tell\_model}
\begin{verbatim}
Answer* tell_model(Server* server, char** models);
\end{verbatim}

\desc{Tells the given files to the server. Note that the server must be
able to find these files in its file system.}

\inputpar{
\begin{description}
\item[server] pointer to the structure discribing the actual server
\item[objects] pointer to a NULL-terminated array of C-strings, containing
the file names which should be loaded by the server.
\end{description}}

\result{An answer struct where {\ttfamily return\_data} is either {\ttfamily yes} or {\ttfamily no}
and where the completion value indicates the result of the operation:
\begin{description}
\item[CB\_OK] operation sucessfull
\item[CB\_ERROR] There was an error while removing, get the errormessages
  calling {\tt get\_errormessages()}
\item[other] see the description in section \ref{completion}
\end{description}}


\subsection{get\_errormessages}
\begin{verbatim}
Error_Messages *get_errormessages(Server *server)
\end{verbatim}

\desc{Gets the errormessages corresponding to the last error. This procedure has
to be called every time CB\_ERROR has been returned by a given procedure.
Otherwise, further messages may be disturbed by the error messages which
are returned first by the server.}

\inputpar{\begin{description}
\item[server] pointer to the structure discribing the actual server
\end{description}}

\result{list of errormessages (see section \ref{errormessages})}


\subsection{ask}

\begin{verbatim}
Answer* ask(Server* pServer,
          char* szQuery,
          char* szAskFormat,
          char* szAnsFormat,
          char* szRBTime);
\end{verbatim}

\desc{Sends the query in the specified format ({\ttfamily szAskFormat}) to the server
and returns the result of the server, which will be represented in the format
given in {\ttfamily szAnsFormat}. The rollback time ({\ttfamily szRBTime}) is
usually {\ttfamily Now}.}

\inputpar{\begin{description}
\item[pServer] a pointer to a server structure
\item[szQuery] the query
\item[szAskFormat] the format of the query (FRAMES or OBJNAMES)
\item[szAnsFormat] the format of the answer (e.g. FRAME, LABEL,...)
\item[szRBTime] rollback time (e.g. Now)
\end{description}}

\result{an answer struct: \begin{description}
\item[sender] the tool that has provided the answer, usually the ID of the server
\item[completion] Completion value indicating the success of the method, e.g. {\ttfamily CB\_OK}, {\ttfamily CB\_ERROR}
\item[return\_data] the result of the query in the specified format, or the string {\ttfamily "nil"}
if there are no results or if there was an error during query processing
\end{description}}


\subsection{ask\_frames}

\begin{verbatim}
Answer* ask_frames(Server *pSserver,
                 char *szQuery,
                 char* szAnsFormat,
                 char *szRBTime)
\end{verbatim}

\desc{As {\ttfamily ask}, but {\ttfamily szAskFormat} is fixed to be {\ttfamily FRAMES}, i.e.
queries have to be given as frames.}

\subsection{ask\_objnames}
\begin{verbatim}
Answer* ask_objnames(Server *pSserver,
                   char *szQuery,
                   char* szAnsFormat,
                   char *cbfoGet,
                   char *szRBTime)
\end{verbatim}

\desc{As {\ttfamily ask}, but {\ttfamily szAskFormat} is fixed to be {\ttfamily OBJNAMES}, i.e.
queries have to be given as object names (or derive expressions).}

\subsection{hypo\_ask}
\begin{verbatim}
Answer* hypo_ask(Server* pServer,
          char* szFrames,
          char* szQuery,
          char* szAskFormat,
          char* szAnsFormat,
          char* szRBTime);
\end{verbatim}

\desc{As {\ttfamily ask}, but first tells the frames given in szFrames to the server, then performs the query
and finally deletes the told frames from the object base.}

\subsection{report\_clients}

\begin{verbatim}
Clients* report_clients(Server *server)
\end{verbatim}

\desc{Returnes a list of all clients connected to the server.}

\inputpar{\begin{description}
\item[server] pointer to the structure describing the actual server
\end{description}}

\result{list of clients or NULL on error}

\subsection{get\_servermessage}

\begin{verbatim}
Answer* get_servermessage(Server* server, char* type);
\end{verbatim}


\desc{Gets a message from the server for the client. This function
is called by {\ttfamily get\_errormessages()}.}

\inputpar{\begin{description}
\item[server] a pointer to a server structure
\item[type] type of the message to be retrieved (e.g. ERROR\_REPORT)
\end{description}}

\result{an answer object with the message or EMPTY\_QUEUE in return\_data}.

\subsection{get\_notification}

\begin{verbatim}
Answer* get_notification(Server* server, int timeout);
\end{verbatim}

\desc{Looks for a notification message. Notification messages are sent
by the server if the client has requested notification on updates on certain views. The
method will wait for a message from the server for the specified time.}

\inputpar{\begin{description}
\item[server] a pointer to a server structure
\item[timeout] time to wait for a message
\end{description}}

\result{an answer object with completion {\ttfamily CB\_NOTIFICATION} when a message was received,
otherwise a completion value, usually {\ttfamily CB\_TIMEOUT}.}


\subsection{stopServer}
\begin{verbatim}
Answer* stopServer(Server* server, char* password);
\end{verbatim}

\desc{Stops the server. Note, that only the user who has started the server
may stop it.}

\inputpar{\begin{description}
\item[server] a pointer to a server structure
\item[password] a password (not used, may be empty)
\end{description}}

\result{the result of the method}


\subsection{LPICall}

\begin{verbatim}
Answer* LPICall(Server* server, char* lpicall);
\end{verbatim}

\desc{Performs a LPI-Call at the server. With LPI (Logic Programming Interface)
one can call ProLog predicates defined in an LPI-Module.}

\inputpar{\begin{description}
\item[server] a pointer to a server structure
\item[lpicall] the predicate to be called
\end{description}}

\result{the result of the method}

\subsection{free*}

\begin{verbatim}
void freeAnswer(Answer* ans);
void freeServer(Server* srv);
void freeClients(Clients* c);
void freeErrorMessages(Error_Messages* err);
\end{verbatim}

\desc{These functions free the allocated memory by the corresponding structures.
Note that memory of all results which are returned by the library methods
have to be freed by the caller.}

\subsection{send\_message}

\begin{verbatim}
Answer send_message(Server *server,
                    char   *method,
                    char   *data)
\end{verbatim}

\desc{This procedure is the most general one and used by most functions mentioned before.
It sends a message of type {\tt method} to the (already connected) CBserver {\ttfamily server}.
{\tt data} is a string containing data expected by the method {\tt method}\footnote{See chapter \ref{serverinterface} and appendix \ref{cha:SyntaxSpec}
of this manual for a complete description of the available methods and their expected data}.
For normal usage of the client library, this function is not necessary.
The more specific functions (e.g. {\tt tellCB, untell, ...}) are more useful.}

\inputpar{\begin{description}
\item[server] pointer to the structure describing the actual server
\item[method] string which defines the type of the message (e.g.\ {\tt TELL})
\item[data] the arguments for the given message type (e.g.\ \tt{["Class Employee with ... end"]})
\end{description}}

\result{an {\tt Answer} structure containing {\tt sender}, {\tt completion} and {\tt return\_data}}



\subsection{CBdecodeString}

\begin{verbatim}
char* CBdecodeString(const char* s);
\end{verbatim}

\desc{Decode a string. ConceptBase encodes all strings with '"' and \\. To
get the plain string, use this function.}

\inputpar{The string to decode.}
\result{The decoded string, it is a duplicate of the input if the input string
is not encoded. The memory allocated by the result has to be freed by the caller.}

\subsection{CBencodeString}

\begin{verbatim}
char* CBencodeString(const char* s);
\end{verbatim}

\desc{Encode a String. ConceptBase encodes all strings with '"' and \\.
Use this function if you want to use Strings in Telos frames.}
\inputpar{The string to encode.}
\result{The encoded string. The memory allocated by the result has to be freed by the caller.}


\subsection{CBgetEncodeLength}

\begin{verbatim}
unsigned CBgetEncodedLength(const char* s);
\end{verbatim}
\desc{Return the length of an encoded string. This function is called
by CBencodeString to allocate the memory of the encoded string.}

\subsection{CBgetLabels}

\begin{verbatim}
char** CBgetLabels(const char* labelList);
\end{verbatim}

\desc{Parse a comma-separated list of labels. ConceptBase returns
sometimes comma-separated list of labels (e.g., for the answer format LABEL).
This function makes an array of strings out of one plain string. This
is a lazy function that will fail to produce a correct result if the
object names contain commata (e.g., {\ttfamily "This, is, a, Telos, object, name, with, commata."}).}
\inputpar{A string with comma-separated-list.}
\result{A NULL-terminated array of strings.}


\section{Compiling and Linking}
\label{sec:compileandlink}

If you want to {\em compile} your source that uses libCB, you have basically
to make sure two things:

\begin{itemize}
\item the header files of ConceptBase are found, and
\item the correct system header files are included in {\ttfamily CBinterface.h}
\end{itemize}

The first item is usually achieved by adding a parameter -I with
the include-directory of your ConceptBase installation to the list of
compiler options. For the second point, you have to define the symbol
LINUX, WIN32 or SOLARIS (usually done with the -D option of the compiler),
depending on the operating system of your client application.

We have used the following compiler flags (with gcc 3.2 on the UNIX-based
systems, and MS Visual C++ 6.0 on Windows):

\begin{description}
\item[Solaris] \verb+-I$(CB_HOME)/include -DSOLARIS+
\item[Linux] \verb+-I$(CB_HOME)/include -DLINUX+
\item[Windows] \verb+-nologo -MT -W3 -GX -O2 -I$(CB_HOME)/include -D "WIN32"+ \\
\verb+-D "NDEBUG" -D "_CONSOLE" -D "_MBCS" -Fo".\\" /Fd".\\" -c +
\end{description}

If you want to {\em link} your application, you have to make sure that libraries
are found by the system (-L option of gcc) and that the library libCB is indeed
linked to your application (-l option). We have used the following linker options:

\begin{description}
\item[Solaris] \verb+-L$(CB_HOME)/sun4/lib -lCB -lnsl -lsocket+
\item[Linux] \verb+-L$(CB_HOME)/linux/lib -lCB+
\item[Windows] \verb+kernel32.lib user32.lib wsock32.lib $(CB_HOME)/windows/lib/libCB.lib+ \\
      \verb+-nologo -subsystem:console -incremental:no -machine:I386+
\end{description}

