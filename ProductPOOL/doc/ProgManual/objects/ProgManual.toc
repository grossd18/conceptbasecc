\contentsline {chapter}{\numberline {1}Server Interface}{2}{chapter.1}
\contentsline {section}{\numberline {1.1}Message Format}{2}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}ipcmessage}{2}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}ipcanswer}{3}{subsection.1.1.2}
\contentsline {section}{\numberline {1.2}Methods Exported by the CBserver}{3}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}TELL}{4}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}UNTELL}{4}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}TELL\_MODEL}{4}{subsection.1.2.3}
\contentsline {subsection}{\numberline {1.2.4}ASK}{4}{subsection.1.2.4}
\contentsline {subsection}{\numberline {1.2.5}HYPO\_ASK}{5}{subsection.1.2.5}
\contentsline {subsection}{\numberline {1.2.6}NEXT\_MESSAGE}{5}{subsection.1.2.6}
\contentsline {subsection}{\numberline {1.2.7}STOP\_SERVER}{6}{subsection.1.2.7}
\contentsline {subsection}{\numberline {1.2.8}REPORT\_CLIENTS}{6}{subsection.1.2.8}
\contentsline {subsection}{\numberline {1.2.9}ENROLL\_ME}{6}{subsection.1.2.9}
\contentsline {subsection}{\numberline {1.2.10}CANCEL\_ME}{6}{subsection.1.2.10}
\contentsline {subsection}{\numberline {1.2.11}GET\_MODULE\_CONTEXT}{6}{subsection.1.2.11}
\contentsline {subsection}{\numberline {1.2.12}LPI\_CALL}{7}{subsection.1.2.12}
\contentsline {chapter}{\numberline {2}Programming Interface for a C Client: libCB}{8}{chapter.2}
\contentsline {section}{\numberline {2.1}Data Structures}{8}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Completion}{8}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Answer}{9}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Server}{9}{subsection.2.1.3}
\contentsline {subsection}{\numberline {2.1.4}Clients}{9}{subsection.2.1.4}
\contentsline {subsection}{\numberline {2.1.5}Error\_Messsages}{10}{subsection.2.1.5}
\contentsline {section}{\numberline {2.2}Functions}{10}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}connect\_CB\_server}{10}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}disconnect\_CB\_server}{10}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}tellCB}{11}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}untell}{11}{subsection.2.2.4}
\contentsline {subsection}{\numberline {2.2.5}tell\_model}{11}{subsection.2.2.5}
\contentsline {subsection}{\numberline {2.2.6}get\_errormessages}{12}{subsection.2.2.6}
\contentsline {subsection}{\numberline {2.2.7}ask}{12}{subsection.2.2.7}
\contentsline {subsection}{\numberline {2.2.8}ask\_frames}{13}{subsection.2.2.8}
\contentsline {subsection}{\numberline {2.2.9}ask\_objnames}{13}{subsection.2.2.9}
\contentsline {subsection}{\numberline {2.2.10}hypo\_ask}{13}{subsection.2.2.10}
\contentsline {subsection}{\numberline {2.2.11}report\_clients}{13}{subsection.2.2.11}
\contentsline {subsection}{\numberline {2.2.12}get\_servermessage}{14}{subsection.2.2.12}
\contentsline {subsection}{\numberline {2.2.13}get\_notification}{14}{subsection.2.2.13}
\contentsline {subsection}{\numberline {2.2.14}stopServer}{14}{subsection.2.2.14}
\contentsline {subsection}{\numberline {2.2.15}LPICall}{15}{subsection.2.2.15}
\contentsline {subsection}{\numberline {2.2.16}free*}{15}{subsection.2.2.16}
\contentsline {subsection}{\numberline {2.2.17}send\_message}{15}{subsection.2.2.17}
\contentsline {subsection}{\numberline {2.2.18}CBdecodeString}{16}{subsection.2.2.18}
\contentsline {subsection}{\numberline {2.2.19}CBencodeString}{16}{subsection.2.2.19}
\contentsline {subsection}{\numberline {2.2.20}CBgetEncodeLength}{16}{subsection.2.2.20}
\contentsline {subsection}{\numberline {2.2.21}CBgetLabels}{16}{subsection.2.2.21}
\contentsline {section}{\numberline {2.3}Compiling and Linking}{17}{section.2.3}
\contentsline {chapter}{\numberline {3}Programming Interface for a C++ Client: libCBview}{18}{chapter.3}
\contentsline {section}{\numberline {3.1}CBclient}{18}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}CBclient}{18}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}CBclient}{18}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}{\tt \~\relax }CBclient}{18}{subsection.3.1.3}
\contentsline {subsection}{\numberline {3.1.4}tell}{19}{subsection.3.1.4}
\contentsline {subsection}{\numberline {3.1.5}untell}{19}{subsection.3.1.5}
\contentsline {subsection}{\numberline {3.1.6}tellModel}{19}{subsection.3.1.6}
\contentsline {subsection}{\numberline {3.1.7}ask}{19}{subsection.3.1.7}
\contentsline {subsection}{\numberline {3.1.8}hypoAsk}{20}{subsection.3.1.8}
\contentsline {subsection}{\numberline {3.1.9}askObjNames}{20}{subsection.3.1.9}
\contentsline {subsection}{\numberline {3.1.10}askFrames}{20}{subsection.3.1.10}
\contentsline {subsection}{\numberline {3.1.11}enrollMe}{20}{subsection.3.1.11}
\contentsline {subsection}{\numberline {3.1.12}cancelMe}{21}{subsection.3.1.12}
\contentsline {subsection}{\numberline {3.1.13}stopServer}{21}{subsection.3.1.13}
\contentsline {subsection}{\numberline {3.1.14}reportClients}{21}{subsection.3.1.14}
\contentsline {subsection}{\numberline {3.1.15}nextMessage}{21}{subsection.3.1.15}
\contentsline {subsection}{\numberline {3.1.16}getErrorMessages}{22}{subsection.3.1.16}
\contentsline {subsection}{\numberline {3.1.17}LPICall}{22}{subsection.3.1.17}
\contentsline {subsection}{\numberline {3.1.18}connected}{22}{subsection.3.1.18}
\contentsline {subsection}{\numberline {3.1.19}operator\ int}{22}{subsection.3.1.19}
\contentsline {subsection}{\numberline {3.1.20}getServerName}{22}{subsection.3.1.20}
\contentsline {subsection}{\numberline {3.1.21}getClientName}{22}{subsection.3.1.21}
\contentsline {section}{\numberline {3.2}CBanswer}{22}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}CBanswer}{23}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}{\tt \~\relax }CBanswer}{23}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}getCompletion}{23}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}getResult}{23}{subsection.3.2.4}
\contentsline {subsection}{\numberline {3.2.5}getRespondingTool}{23}{subsection.3.2.5}
\contentsline {section}{\numberline {3.3}CBerror}{23}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}CBerror}{23}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}{\tt \~\relax }CBerror}{24}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}getErrorMessage}{24}{subsection.3.3.3}
\contentsline {subsection}{\numberline {3.3.4}getAllErrorMessages}{24}{subsection.3.3.4}
\contentsline {subsection}{\numberline {3.3.5}getNextError}{24}{subsection.3.3.5}
\contentsline {chapter}{\numberline {4}Processing of Telos Frames: libtelos}{25}{chapter.4}
\contentsline {section}{\numberline {4.1}fragment.h and te\_callparser.h}{25}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Typedef: BindingList}{25}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Typedef: ObjectIdentifier}{25}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Typedef: te\_ClassList}{26}{subsection.4.1.3}
\contentsline {subsection}{\numberline {4.1.4}Typedef: AttrClassList}{26}{subsection.4.1.4}
\contentsline {subsection}{\numberline {4.1.5}Typedef: SpecObjId}{26}{subsection.4.1.5}
\contentsline {subsection}{\numberline {4.1.6}Typedef: SelectExpB}{26}{subsection.4.1.6}
\contentsline {subsection}{\numberline {4.1.7}Typedef: Restriction}{26}{subsection.4.1.7}
\contentsline {subsection}{\numberline {4.1.8}Typedef: ObjectSet}{26}{subsection.4.1.8}
\contentsline {subsection}{\numberline {4.1.9}Typedef: PropertyList}{26}{subsection.4.1.9}
\contentsline {subsection}{\numberline {4.1.10}Typedef: AttrDeclList}{27}{subsection.4.1.10}
\contentsline {subsection}{\numberline {4.1.11}Typedef: te\_SMLfragmentList}{27}{subsection.4.1.11}
\contentsline {subsection}{\numberline {4.1.12}Typedef: FrameParseOutput}{27}{subsection.4.1.12}
\contentsline {subsection}{\numberline {4.1.13}Typedef: ClassListParseOutput}{27}{subsection.4.1.13}
\contentsline {subsection}{\numberline {4.1.14}te\_frame\_parser}{27}{subsection.4.1.14}
\contentsline {subsection}{\numberline {4.1.15}te\_classlist\_parser}{28}{subsection.4.1.15}
\contentsline {subsection}{\numberline {4.1.16}FragmentToString}{28}{subsection.4.1.16}
\contentsline {subsection}{\numberline {4.1.17}DestroySMLfrag}{28}{subsection.4.1.17}
\contentsline {subsection}{\numberline {4.1.18}Destroy\_ClassList}{28}{subsection.4.1.18}
\contentsline {section}{\numberline {4.2}te\_access.h}{28}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Structure: te\_AttrDecl}{28}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Typedef: TAttrDecl}{29}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Typedef: PAttrDecl}{29}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4}Typedef: VTelos}{29}{subsection.4.2.4}
\contentsline {subsection}{\numberline {4.2.5}Typedef: PVTelos}{29}{subsection.4.2.5}
\contentsline {subsection}{\numberline {4.2.6}Typedef: AVTelos}{29}{subsection.4.2.6}
\contentsline {subsection}{\numberline {4.2.7}Structure: te\_TelosReport}{29}{subsection.4.2.7}
\contentsline {subsection}{\numberline {4.2.8}Typedef: TReport}{29}{subsection.4.2.8}
\contentsline {subsection}{\numberline {4.2.9}Typedef: PReport}{29}{subsection.4.2.9}
\contentsline {subsection}{\numberline {4.2.10}vt\_createByFragment}{30}{subsection.4.2.10}
\contentsline {subsection}{\numberline {4.2.11}vt\_create}{30}{subsection.4.2.11}
\contentsline {subsection}{\numberline {4.2.12}vt\_destroy}{30}{subsection.4.2.12}
\contentsline {subsection}{\numberline {4.2.13}rep\_create}{30}{subsection.4.2.13}
\contentsline {subsection}{\numberline {4.2.14}rep\_destroy}{31}{subsection.4.2.14}
\contentsline {subsection}{\numberline {4.2.15}getValueOfLabel}{31}{subsection.4.2.15}
\contentsline {subsection}{\numberline {4.2.16}getCategories}{31}{subsection.4.2.16}
\contentsline {subsection}{\numberline {4.2.17}destroyASZ}{31}{subsection.4.2.17}
\contentsline {section}{\numberline {4.3}te\_cursor.h}{31}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Structure: te\_framecursor}{31}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Typedef: TFrameCursor}{32}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Typedef: PFrameCursor}{32}{subsection.4.3.3}
\contentsline {subsection}{\numberline {4.3.4}te\_createCursor}{32}{subsection.4.3.4}
\contentsline {subsection}{\numberline {4.3.5}te\_destroyCursor}{32}{subsection.4.3.5}
\contentsline {subsection}{\numberline {4.3.6}te\_resetFrame}{32}{subsection.4.3.6}
\contentsline {subsection}{\numberline {4.3.7}te\_nextFrame}{32}{subsection.4.3.7}
\contentsline {subsection}{\numberline {4.3.8}te\_retOID}{33}{subsection.4.3.8}
\contentsline {subsection}{\numberline {4.3.9}te\_resetOmega}{33}{subsection.4.3.9}
\contentsline {subsection}{\numberline {4.3.10}te\_nextOmega}{33}{subsection.4.3.10}
\contentsline {subsection}{\numberline {4.3.11}te\_retOmega}{33}{subsection.4.3.11}
\contentsline {subsection}{\numberline {4.3.12}te\_resetIsA}{33}{subsection.4.3.12}
\contentsline {subsection}{\numberline {4.3.13}te\_nextIsA}{33}{subsection.4.3.13}
\contentsline {subsection}{\numberline {4.3.14}te\_retIsA}{34}{subsection.4.3.14}
\contentsline {subsection}{\numberline {4.3.15}te\_resetIn}{34}{subsection.4.3.15}
\contentsline {subsection}{\numberline {4.3.16}te\_nextIn}{34}{subsection.4.3.16}
\contentsline {subsection}{\numberline {4.3.17}te\_retIn}{34}{subsection.4.3.17}
\contentsline {subsection}{\numberline {4.3.18}te\_resetAttrDecl}{34}{subsection.4.3.18}
\contentsline {subsection}{\numberline {4.3.19}te\_nextAttrDecl}{34}{subsection.4.3.19}
\contentsline {subsection}{\numberline {4.3.20}te\_resetCategory}{34}{subsection.4.3.20}
\contentsline {subsection}{\numberline {4.3.21}te\_nextCategory}{35}{subsection.4.3.21}
\contentsline {subsection}{\numberline {4.3.22}te\_retCategory}{35}{subsection.4.3.22}
\contentsline {subsection}{\numberline {4.3.23}te\_resetProperty}{35}{subsection.4.3.23}
\contentsline {subsection}{\numberline {4.3.24}te\_nextProperty}{35}{subsection.4.3.24}
\contentsline {subsection}{\numberline {4.3.25}te\_filterPropertyByCategory}{35}{subsection.4.3.25}
\contentsline {subsection}{\numberline {4.3.26}te\_filterPropertyByCategories}{35}{subsection.4.3.26}
\contentsline {subsection}{\numberline {4.3.27}te\_filterPropertyByLabel}{36}{subsection.4.3.27}
\contentsline {subsection}{\numberline {4.3.28}te\_retLabel}{36}{subsection.4.3.28}
\contentsline {subsection}{\numberline {4.3.29}te\_retValue}{36}{subsection.4.3.29}
\contentsline {chapter}{\numberline {5}Programming Interface for a Java Client}{37}{chapter.5}
\contentsline {section}{\numberline {5.1}Communication with ConceptBase: i5.cb.api}{37}{section.5.1}
\contentsline {section}{\numberline {5.2}Parsing Telos Frames: i5.cb.telos.frame}{38}{section.5.2}
\contentsline {section}{\numberline {5.3}ObjectBaseInterface: i5.cb.telos.object}{38}{section.5.3}
\contentsline {chapter}{\numberline {A}Example C Client}{39}{appendix.A}
\contentsline {chapter}{\numberline {B}Syntax Specifications}{42}{appendix.B}
\contentsline {section}{\numberline {B.1}Syntax Specification for IPC messages}{42}{section.B.1}
\contentsline {section}{\numberline {B.2}Syntax Specification for IPC answers}{43}{section.B.2}
