
                   ConceptBase.cc
  
                  InstallationGuide
                     2018-01-26


                         by
                  The ConceptBase Team
       http://conceptbase.sourceforge.net/cbteam.html

                 http://conceptbase.cc






CONTENTS
========

1. System Requirements
2. Files
3. Installation
4. Starting ConceptBase
5. Important Additional Remarks
6. Memory Consumption
7. Uninstall ConceptBase
8. Contact




======================
1. System Requirements
======================

The ConceptBase.cc system (short: ConceptBase) consists of
 - CBserver: the ConceptBase server, essentially a persistent database server implementing the Telos data model
 - CBIva: a window-based client to edit the database maintained in a CBserver
 - CBGraph: a graphical editor for the CBserver
 - CBShell: a command shell for interacting with the CBserver

The full ConceptBase system (CBserver+CBIva+CBGraph+CBShell) is pre-compiled for the following platforms:
 - Linux on i386
 - Linux on AMD64/EMT64 (64bit CPU from AMD or Intel)
The compilation of the CBserver on other Unix-style platforms should be possible by following
the instructions included with the ConceptBase sources at
   https://sourceforge.net/projects/conceptbase/files/Sources/
The CBserver can also be compiled for Windows though we have no detailed instructions
on how to do it.

The ConceptBase client (CBIva+CBGraph+CBShell) is available for
all platforms that have a compatible Java Runtime Environment (see below).

The hardware requirements are quite low: 64MB main memory and 50MB hard disk space.
Large databases require more main memory and hard disk space. See section 6 for 
more details.

ConceptBase.cc should also run on other platforms such as Windows, FreeBSD or Mac-OS.
We do however not provide CBserver binaries for those platforms. You may want to
compile ConceptBase.cc from its sources on them.
Note however, that ConceptBase.cc is fully operational on any platform with
a compatible Java Runtime Environment! If the CBserver binaries are not
shipped for the platform, ConceptBase.cc will use a public CBserver. 

Windows 10: Since April 2017, you can install a Linux sub-system and then also start
the CBserver locallly on your WIndows 10 machine.
See http://conceptbase.sourceforge.net/CB-WinLinux.html
  


1.1 Java
========

ConceptBase.cc requires a Java Runtime Environment (JRE) compliant to Java 6.
The successor versions Java 7 and 8 should also work. Older JRE's such as Java 1.4
and Java 5 also work but are deprecated since they are no longer maintained.



1.2 Libraries
=============

The ConceptBase server requires the following dynamic libraries. Most of them should already
be installed on your computer. You can check this after installation on Unix/Linux by
the command

   ldd $CB_HOME/`CBvariant`/bin/CBserver

after adding $CB_HOME/bin to your search path.


ConceptBase is distributed under a FreeBSD-style copyright license. See file
http://conceptbase.sourceforge.net/CB-FreeBSD-License.txt.

If you plan to compile ConceptBase from its sources, then download the source files
from
  http://sourceforge.net/projects/conceptbase/files/Sources/CBPOOL.zip
and follow the instructions in the file CBPOOL/AdminPOOL/CB-developer-config.txt
of the extracted source pool directory.



  
=====================
2. Installation Files
=====================


CBinstaller.jar:    ConceptBase installer program including the ConceptBase binaries,
                    documentation, and examples
InstallationGuide:  this file



===============
3. Installation
===============

You can download the ConceptBase system from
  http://conceptbase.sourceforge.net/CB-Download.html
The instructions below refer to the full ConceptBase system. You need to download
the file:
  - CBinstaller.jar (the ConceptBase installer)



3.1 Using the CBinstaller Program
=================================

To run the CBinstaller program, you must have the Java Runtime
Environment Version 6 or newer. 

Download CBinstaller.jar from the ConceptBase download page.
Start the CBinstaller either by a double-click on the
file CBinstaller.jar, or by typing

  java -jar CBinstaller.jar

in a command/shell window.

A window should pop up. The CBinstaller program contains the
ConceptBase binaries. You thus only have to press
the activated "Install" button (green color) to install it
to the installation directory. You may change it if you want to
have it installed at a different location. 

You can un-select variants/components that you do not want to install. We
recommend to leave all variants/components selected! 
Under Windows 10, you should also install the Linux64 binaries since
there is a Linux sub-system available for Windows to run the CBserver,
see also: http://conceptbase.sourceforge.net/CB-WinLinux.html

By clicking on the "Install" button, the files will be extracted from
the installation archive and installed into the specified directory.
Furthermore, the environment variable CB_HOME will be set to the installation
directory in the script files cb* in the installation directory.

Subsequently, we refer by <CB_HOME> to the absolute path of the directory into
which you installed ConceptBase. Avoid directory names with blanks in the path! 
This can cause problems when starting the ConceptBase user interface. 

We recommend to include CB_HOME in the search path of your command shell. For example,
if you use bash as your command shell in Unix/Linux, then add the following line in
the file .bashrc:
   export CB_HOME=$HOME/conceptbase  
   export PATH=$CB_HOME:$PATH



3.2 Manual Installation 
=======================

If for some reason the CBinstaller method fails or if you prefer manual installation.
then follow these steps:

a. Download the file CBinstaller.jar from
   https://sourceforge.net/projects/conceptbase/files/Binaries/ 
   Rename the file to CBinstaller.zip

b. Create an installation directory (e.g. $HOME/conceptbase or c:\conceptbase).
   Unzip the file CBinstaller.zip into this installation directory (referred to as <CB_HOME>).

c. Edit in directory <CB_HOME> the files cbserver[.bat], cbshell[.bat], cbgraph[.bat]
   and cbiva[.bat] by replacing the value of the variable
   CB_HOME with the path of your installation directory, i.e. the directory into which you unpacked
   the zip file:
   Linux/Unix/MacOS:
               replace the string after "CB_HOME:=" by the path of your installation
               directory, e.g. $HOME/conceptbase
   Windows:    replace the string after "CB_HOME=" by the path of your installation
               directory, e.g. c:\conceptbase

c. Linux/Unix/MacOS: Execute "sh make-executable" in the installation directory.



3.3 Configuring your desktop for ConceptBase
============================================

See 
   http://conceptbase.sourceforge.net/CB-Mime.html
for instructions on how to integrate ConceptBase with your desktop.



=======================
4. Starting ConceptBase
=======================

ConceptBase uses a client-server architecture, i.e. the user interface
CBiva/CBGraph/CBShell and the server CBserver are separate programs that can also
run on different computers.
In fact, if you use ConceptBase in a multi-user setting, then you will want to
start the CBserver on a separate computer, ideally a powerful server with a lot
of main memory. The client CBiva does not require a lot of computing resources and
can run on a standard desktop machine.

There are three principal ways to start CBiva and CBserver. Method 1 uses the standard scripts
providing all options for setting parameters. This is the preferred way for
regular work with the system. Method 2 is a "quick" way to start the programs with
default settings for the CBserver. 


4.1 Method 1 - Standard Way of Starting ConceptBase
===================================================


4.1.1 Starting the ConceptBase Server
-------------------------------------

The following steps are currently only supported for Linux platforms.
All other platforms use a public ConceptBase server.

Open a shell/command window, change to the installation directory <CB_HOME>,
and start the server thye cbserver script, e.g. 

    cbserver -port 4001 -d MYDB


The number 4001 is the port (or "socket") number that is used to connect
 clients to the server process. You can specify any number between 2000 and
65535. The default value is 4001 (if you omit the '-p' option). The parameter
-d specifies the database (sometimes called "application" in the user manual)
to be loaded by the server at startup time. If a path is omitted (as above)
then the database is loaded from the current directory. If there is no
database with the specified name (here:MYDB) then it is created and initialized
with the system classes of O-Telos, the object model of ConceptBase.
After successfully loading the CBserver reports

          CBserver ready on host ...

with some additional information.

Please note that no two servers can use the same port number on the same
machine. In this case you get the error message

	IPC Error: Unable to bind socket to name

and the server won't start up. You should then either start the server on a
different machine or use another port number.

The list of other command line parameters can be obtained by
including the parameter -help. Specified parameters must be separated by blanks.
See also the ConceptBase User Manual in <CB_HOME>/doc/UserManual for the 
full specification of command line parameters.

You can redirect the output of the CBserver to a log file by a command like
    cbserver -port 4001 -d MYDB  > logfile1.txt
The logfile contains textual output of the CBserver indicating when which
operation was called. The amount of output can be controlled by the
CBserver parameter -t (tracemode). 



4.1.2 Starting the ConceptBase Usage Environment CBiva
------------------------------------------------------

The Java-based graphical user interface CBiva can be started in the 
ConceptBase installation directory via the command

    cbiva

CBiva will try to start a local CBserver on Linux and Windows 10.
On other platforms, it attempts to connect to a public CBserver if
configured accordingly.

If starting the application was successful, the main window of CBiva
should appear.

On Windows, you can start CBiva also by double-clicking the file cbiva.bat
in your ConceptBase installation directory.



4.1.3 Connecting CBserver and CBiva
-----------------------------------

You can connect CBiva to any CBserver running on the local host or on other host
reachable via the Internet. Any prior connection will be terminated before
the new connection is established.

The new connection is initiated by selecting menu item "Connect"
from the File menu of CBiva. In the appearing interaction window the
host name and port number of the CBserver must be specified and as a result
the connection is established.

Example:
           1. 	start server on machine alpha with port number 4002 (see above)
           2. 	start CBiva on machine beta (see above)
           3. 	connect usage environment on beta to the server on alpha
              	by selecting Connect CB server and specifying
	  		hostname   : alpha
	  		portnumber : 4002


Whenever a new connection from CBiva to a CBserver is created a
possibly existing 'old' one is closed. Please note that the machines
alpha and beta can be located anywhere in the Internet. You can also start
both processes on the same machine (use 'localhost' as hostname). 
To connect through a firewall, we recommend the use of secure shell tunnels.

Note: If you use ConceptBase on Windows7, Windows 10 (32bit) or Mac OS-X, then CBiva shall automatically
attempt to connect to a public ConceptBase server, since there are no recent binaries
of the ConceptBase server for these Windows versions or Mac OS-X. 
Windows 10 (64bit) can run the CBserver when you configure it according to
http://conceptbase.sourceforge.net/CB-WinLinux.html


4.2 Method 2 - Starting ConceptBase via CBiva
=============================================

If you run the user interface and the CBserver on the same computer, e.g. you
desktop PC, then you can simplify the start procedure by just starting 
CBiva (either with method 1 or method 2) and then starting the CBserver 
via CBiva using the menu option File/Start CBserver. A pop-up window allows
you to change the options of the CBserver. This way is suitable in single-user
settings. If you start the CBserver in this way, you do not need to connect
CBiva to the CBserver. They are automatically connected. See the ConceptBase 
user manual for more details.

On Windows/MacOS, the user interface CBiva automatically connects to a public
ConceptBase server located at the University of Skovde. You can also run the
ConceptBase server on a local Linux server in your network and connect to it.

Method 2 is convenient but less flexible than method 1. For example, you cannot 
redirect to output of the CBserver to some log file and a crash of CBiva (the unlikely
that may be) also affects the CBserver.



===============================
5. Important additional remarks
===============================

A CBserver may only be stopped by a client if the name of the user of the
client is the same as the one who started the CBserver. This behavior is
not an error but a security measure. Use the '-a' option of the CBserver
to assign another user as administrator of the CBserver.
A rude method to stop a CBserver
is by pressing CTRL-C in the command window where you started it, or by
killing the process. In theses cases, you might loose data.

If the CBserver and/or CBiva do not start up under Unix/Linux, you
might have to set the execution flag on the script and binary files.
You can do so by typing
     cb $CB_HOME
     sh ./make-executable


We hope the system is useful for your application. We recommend to start with
the ConceptBase tutorials (in <CB_HOME>/doc/Tutorial) for getting used to the
language and the system.

ConceptBase users are invited to join the ConceptBase Forum. This shared workspace
provides additional documentation, software, and many examples.
More details are available at
  http://conceptbase.cc/CB-Forum.html

A bibliography on the system is available via the following URL:
http://www-i5.informatik.rwth-aachen.de/CBdoc/cblit.html. 



=====================
6. Memory Consumption
=====================

An atomic statement called P-fact in ConceptBase consumes about 1000 bytes
of memory. A typical concept is described by about 10 P-facts (2-4 attributes).
Hence, 1GB can hold around 100.000 concepts. On top of the memory for P-fact,
ConceptBase needs some memory for intermediate results of queries. So, with
2GB you should be able to handle models with about 100.000 concepts.

That may sound low in comparison to database systems but keep in mind that 
ConceptBase is not for storing data but for storing concepts that are part of models.
For example, a database schema with 1000 different classes is regarded as quite
large. In ConceptBase, you can handle schemas that are a hundred times larger.
Our largest models that we encountered had around 10.000 concepts. So, well
in the reach of installed memory of a typical desktop PC.

The object store of ConceptBase.cc uses 32bit integers for object identifiers.
So while a 64bit operating system allows you to handle much larger ConceptBase
databases than 32bit operating systems, you cannot have more than 2^32
P-facts at the moment, i.e. roughly 400000 concepts with attributes.
We might switch to 64bit object identifiers if applications
are limited by this data structure.


========================
7. Uninstall ConceptBase
========================

There is no specific uninstall program for ConceptBase.cc. All files are stored
in the installation directory of ConceptBase, e.g. c:\conceptbase under Windows
or $HOME/conceptbase under Linux. Make sure that ConceptBase is not running
and then simply remove the whole installation directory. You may also want to
to remove shortcuts to ConceptBase on your desktop.

ConceptBase stores a file .CBjavaInterface in your home directory. You can
safely delete this file as well. ConceptBase itself stores no data in the Windows
Registry, but if you followed the instructions to integrate the ConceptBase client with
the desktop, then this will associate the filetype *.GEL to the CBGraph program
of ConceptBase.

Do not use the Windows Uninstall (add/remove programs). And BEWARE of third-party
uninstallers that claim to un-install ConceptBase. They are likely installing some
unwanted programs on your computer!




==========
8. Contact
==========

Manfred Jeusfeld
University of Skovde / IIT
Box 408
S-54128 Skovde, Sweden 

email: manfred.jeusfeld@acm.org
http://conceptbase.cc






