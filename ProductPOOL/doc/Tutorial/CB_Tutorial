*************************************************************
                C O N C E P T B A S E

              RWTH Aachen, Informatik V
               Prof. Dr. Matthias Jarke
                    Ahornstr. 55
                 52056 Aachen, Germany
               Tel.: ++49+241 80 21 501
               Fax   ++49+241 80 22 321
      email: CB@picasso.informatik.rwth-aachen.de
http://www-i5.informatik.rwth-aachen.de/CBdoc/cbflyer.html

*************************************************************

                ConceptBase Tutorial

*************************************************************



1.  Introduction
================

This Tutorial gives a beginners introduction into Telos and ConceptBase. 
Telos is a formal language for representing knowledge in a wide
area of applications, e.g. requirements and process-modelling. 
It integrates object-oriented and deductive features into a 
logical framework.
ConceptBase is an experimental deductive object base management
system, based on the Telos data model. It is designed to store and 
manipulate a database of Telos-objects. 
The tutorial is organized as follows:
The next section gives a short introduction into the architectural 
organization of the ConceptBase system and describes the
necessary steps to start the system.
Section three explains some basic features of Telos and ConceptBase
using a simple example.
The last chapter contains solutions to the exercises.


Please note:

The objective of this tutorial is to give a novice user a first intuitive 
feeling on how to work with CB and how to build own models, not
to mention all the features of Telos and ConceptBase or describe
the semantics of Telos.



2. First Steps
==============


2.1 Overview of the Architecture of ConceptBase
===============================================

ConceptBase is organized in a client/server architecture. The server 
manages the object base while the client may be any user-defined
application program. A standard client -the CBworkbench- is 
distributed with the ConceptBase system.
The communication between server and client is realized through a 
standard network (Internet).
The connection is established by the server and is identified by a unique
number, the so called port number.
The object base maintained by the server is called application. 
Every application is stored in a seperate directory where the name of 
the application is the name of this directory.



2.2 Starting the ConceptBase Server
===================================

The first thing to do is to start a ConceptBase server:

Exercise 2.1:
a) Get a description of all possible command-line parameters of 
   the ConceptBase server by entering 	
     $CB_HOME/bin/CBserver -h

b) Start a ConceptBase server from a C-Shell, loading the
   database "TutApp" and portnumber 5544

The server will start running immediately. If the application TutApp 
doesn't exist, a new application will be created before loading the 
application into the server. Then the copyright notice and  
parameter-settings are displayed, followed by a message which 
contains hostname and portnumber of the ConceptBase server you 
have just started. These two informations are used to specify a server. 
The host is the one, you are currently logged on to and the portnumber 
is set by the -p parameter. The portnumber you specify must be unique 
on the host where  ConceptBase should run. If this number is already 
in use by another server, the error-message

IPC Error: Unable to bind socket to name 

appears and the server stops. 
In this case restart the server with another portnumber.


2.3 Starting the ConceptBase User Interface
===========================================

Clients can communicate with a server through the ConceptBase Usage
Environment. The interface contains several tools which can be invoked 
from the so-called ConceptBase Workbench
Start the Usage Environment by entering 

$CB_HOME/bin/CBinterface &

on a second C-Shell. After a few seconds a window will appear which
is titled CBworkbench. It consists of two main windows and a statusline
and offers several function keys and menu-items.  A complete description of 
these menues is given in section 3.1 of the User Manual.

Exercise 2.2:
Try to establish a connection between the CBworkbench and 
the server you have started under 2.1

After the connection is established a message appears in
the Protocol window, indicating that the connection was successfull and
the first field of the status-line contains the status "connected".



3. The Example Model
====================

In this section the use of the basic Tools of ConceptBase and basic 
concepts of Telos will be illustrated  by modelling the following simple 
scenario:

A company has employees, some of them being managers. Employees 
have a name and a salary which may change from time to time. They 
are assigned to departments which are headed by managers. The boss 
of an employee can be derived from his department and the manager of 
that department. No employee is allowed to earn more money than his 
boss.


The model we want to create contains two levels: the class level 
containing the classes Employee, Manager and Department and 
the token level which contains instances of these 3 classes.

 
3.1 Editing Telos Objects using the Telos Editor
================================================


3.1.1 The Class Level
=====================

The first thing to do is to create the three classes used in this example: 
Employee, Manager and Department.

Enter the following Telos object into the top-window of the CBworkbench:

Employee in Class 
end

This is the declaration of the class Employee, which will contain 
every employee as instance. Employee is declared as instance of the 
systemclass "Class", because it is on the class level of our example, 
i.e. it is intended to have instances.

 
To add this object to the object base, press the "Tell" button. 
If no syntax error occurs and the semantic integrity of the object base 
isn't violated by this new object it will be added to the object base.

Then we add the class Manager. 
Managers are also employees, so the class Manager is declared as a
specialization of Employee using the keyword isA:

Manager in Class isA Employee 
end

Press the "Clear" button to clear the editor field.
Enter this object and add it to the object base by telling it.
The final class that should be added is the class Department. 

Exercise 3.1
Define a class Department  and add it to the object base. 

At this point we have only added some new classes to the object 
base, but have told nothing about the so called "attributes" of these 
classes.  The modification of the classes we have just entered is the 
next task.


3.1.2 Defining Attributes of Classes
====================================

As mentioned in the description of the example-model, the employee-class 
has several attributes. These attributes have not been told  yet. 
To add them, we need to modify the Telos frame describing the class 
"Employee".

The Editor-window has an option called "Overwrite Telos editor".
If this option is acivated, an automatic "Clear" will be performed
before the contents of the editor window is changed. You can 
activate it via the "Options"-menu. 

Exercise 3.2
Acticate "Overwrite Telos editor" and load the Telos-Frame defining 
"Employee".

You might have recognized, that the frame has changed in a way: 
the class descriptor "Individual" has been added at the first position. 
An explanation for this is given in chapter 2.1 of the User Manual. 
Please ignore this for the moment.

Modify the Employee-Frame as follows:

Individual Employee in Class with
attribute
	name: String;
	salary: Integer;
	dept: Department;
	boss: Manager
end 

To store the modified Employee-Frame in the object base, 
select Tell .

Now you have added the 4 attributes to the class Employee. 
They are of the category "attribute" and  their labels are: 
"name", "salary", "dept", and "boss".  The classes "Department"
and "Manager" are user-defined, while String and Integer 
are builtin classes of ConceptBase. ConceptBase strings are
delimited by double quotation marks.

Notice that these attributes are also available for the class 
Manager, because this class is a subclass of Employee
(i.e. Telos offers attribute inheritance, see also chapter 2.1 
of the User manual, "Specialization axiom").


Exercise 3.3 
The Department-class has only one attribute: the manager, 
who leads the department. Add this attribute to the class Department. 
The label of this attribute should be "head".

Now we have completed the class-level of our example. 
The next step is to add instances of our classes to the object base.


3.1.3 The Token Level
=====================

The company we are modelling consists of the 4 departments
Production, Marketing, Administration, and Research.

Every employee working in the company belongs to
a department. The employees will be listed later, apart 
from the managers of the departments:


department       |   head
------------------------------
Production       |   Lloyd
Marketing        |   Phil
Administration   |   Eleonore
Research         |   Albert



The next step is to establish the link between the department Production 
and its manager Lloyd.
How to do this is described in the following chapter.


3.1.4 Defining Attributes of Tokens
===================================

At first let's have a look at the department class, defined in exercise 3.3:

Department in Class with
	attribute
		head: Manager 
end 

There is a link between "Department" and "Manager" of category 
"attribute" with label "head" at the class-level. Now we have to establish a  
link between"Production" and "Lloyd" of category "head" at the token-level.
The "label" of  this link must be a unique name for all links with the
source object "Production". We choose "head_of_Production" as name.   

The resulting Telos frame is:

Production in Department with
  head
    head_of_Production : Lloyd
end 



Exercise 3.4

a) Add the frames for LLoyd, Phil, Eleonore and Albert to the 
   object base.
b) Add the Telos frames for Production, Marketing, Administration, 
   and Research and the links between the departments and their 
   manager to the object base.
c) The four managers have the following salaries:
	Lloyd		100000
	Phil		120000
	Eleonore	20000
	Albert		110000

  Add this information to the object base.
  Use "LloydsSalary", "PhilsSalary", etc. as labels.
  (Remember that you can load an existing object from the object base into
  the Telos Editor by using "Load frame".)

At this point it is important to recognize, that  attributes specified at the
class level do not need to be instantiated at the instance level. On the other 
hand an instance of a class containing an attribute may contain several 
instances of this attribute.

Example:
George in Employee with
	name
		GeorgesName: "George D. Smith"
	salary
		GeogesBaseSalary : 30000;
		GeorgesBonusSalary : 3000
end George

The attribute "dept" and "boss" have no instances, while 
"salary" was instantiated twice.

To complete the token level, we have to enter some employees into
the object base.

Exercise 3.5
Add the following employees to the object base:

employee      department      salary

Michael       Production      30000
Herbert       Marketing       60000
Maria         Administration  10000
Edward        Research        50000

Use "MichaelsDepartment" etc. as labels for the attributes.

Now the first step in building the example application is finished. 
The next chapter describes a basic tool of the usage environment
which can be used for inspecting the object base: the GraphBrowser.


3.2 The Graph Browser
=====================

To start the GraphBrowser, choose the menu item "Browsing" from 
the Workbench and select "Browse Graphical". An interaction window
appears, where you must enter an object name. The GraphBrowser
starts up displaying this object. By clicking the left mouse-button, every
displayed object can be selected. A selected object can be moved by 
dragging the object with the right mouse-button. If an object is selected 
the following menu items are available:

Erase -		deletes the selected object (node or link) from the display.

Any - 			displays all objects connected to the selected object by 
			a link corresponding to a userdefined specification of 
			orientation and label 

Show Attributes - 	displays all the direct attributes (not the inherited ones);

Show Instances - 	displays all the direct instances (not the derivable ones); 
			not available if the selected object is a link;

Show Classes - 		displays all classes the selected object is an instance of; 
			not available if the selected object is a link;

Show Subclasses - 	displays one level of subclasses (i.e. subclasses of a 
			subclass of the selected node are not displayed); 
			not available if the selected object is a link;

Show Superclasses -	displays one level of superclasses (i.e. superclasses of
			a superclass of the selected node are not displayed); 
			not available if the selected object is a link;

Help - 			a window appears containing a description of the 
			GraphBrowser's facilities;

quit -			terminates the GraphBrowser.


If no object is  selected, there is an option Add Node. Choosing it, 
a new object with no connection to the existing graph is introduced at the 
position of the last left mouse click.

Exercise 3.6
Start a GraphBrowser, entering "Employee" as initial object and
experiment with the menu options available.


3.3 Adding Deductive Rules 
==========================

At this point you have made some experiences with the editing- and 
browsing-facilities of the ConceptBase Usage Environment and the
Telos language. This chapter gives an introduction into adding rules 
and integrity constraints to an object base, using CBL, the Concept
Base predicative sublanguage.

Until now we have never instantiated the boss-attribute of an employee.
The boss can be derived from the department the employee is assigned 
to and the head of this department. So its obvious to define the instances
of the boss-attribute by adding a rule to the Employee-Frame. 

At first we'll give a short introduction into the syntax of CBL. The exact 
syntax is given in appendix A2.2 ("Syntax of the Assertion Language") of
the user manual.

A deductive rule has the following format:
forall x1/c1 x2/c2 ... xn/cn Rule ==> lit(a1,...,am)
where "Rule" is a CBL formula and the xi's are variables bound to the class ci, 
lit is a literal of type 1-3 (see below) and the variables among the ai's 
are exactly x1,..,xn.

To compose the formula for a deductive rule or integrity constraint the
following literals may be used:

1) 	(x in c)
	The object x is an instance of class c.

2) 	(c isA d)
	The object c is a specialization (subclass) of d 

3) 	(x l y)
	The object x has an attribute to object y and this relationship 
	is an instance of an attribute category with label l. Structural 
	integrity demands that the label l belongs to an attribute of a 
	class of x. 

In order to avoid ambiguity, neither "in" and "isA" nor the logical 
connectives "and" and "or" are allowed as attribute labels.

The next literals are second class citizens in formulas. In contrast to the 
above literals they cannot be assigned to classes of the Telos object base. 
Consequently, they may only be used for testing, i.e. in a legal formula their 
parameters must be bound by one of the literals 1 - 3.

4) 	(x < y),( x > y), (x <= y), (x >= y), (x = y), (x <> y) 

5) 	The objects x and y are the same:
	(x == y)

"and" and "or" are allowed as infix operators to connect subformulas.
Variables in formulas can be quantified by 

forall x/c or exists x/c, 

where c is a class, i.e. the range of x is
the set of all instances of the class c.

Of course the constants appearing in formulas must be names of
existing objects in the object base or of type Integer, Real or 
String. Also for the attribute predicates (x l y) occuring in the formulas 
there must be a unique attribute labelled l of one class c of x in the 
object base. For the exact syntax refer to Appendix 2.2 of the user manual.

Now we'll give a first example of a deductive rule. The following rule 
defines the boss of an employee:

Employee with
   rule
      BossRule : $ forall e/Employee m/Manager
                   (exists d/Department
                     (e dept d) and (d head m)) 
                   ==> (e boss m) $
end 


Please note that the text of the formula must be enclosed in "$" and that
this deductive rule is legal, because all variables appearing in the conlusion
literal (e,m) are universal (forall) quantified. The logically equivalent formula

   forall e/Employee m/Manager d/Department
        (e dept d) and (d head m) 
        ==> (e boss m)

will not be accepted, because the syntax is violated.

Exercise 3.7
Add  BossRule to the object base.


3.4 Adding Integrity Constraints
================================

A legal integrity constraint is an arbitrary CBL formula. 
Its attribute category is "constraint". 
If , e.g, the formula has the format
forall x1/c1 x2/c2 ... xn/cn Rule ==> lit(a1,...,am)
it is not required, that the variables among a1,..,am are exactly x1,..,xn
as it is for deductive rules.

The following integrity constraint specifies the rule that no Manager 
should earn less than 50000:

Manager with
   constraint
      earnEnough: $ forall m/Manager x/Integer
                        (m salary x) ==> (x >= 50000) $
end 

(Please note that our example model NOT satisfies this
 constraint, because Eleonore earns only 20000. If you
 use 20000 instead of 50000, the model satisfies this constraint
 and the telling will be successfull.)

Exercise 3.8
Define an integrity constraint stating that no employee
is allowed to earn more money than any of her/his bosses. (The constraint 
should work on each individual salary, not on the sum).

In the subdirectory RULES+CONSTRAINTS of the example directory
there is a more extensive example concerning deductive rules and 
integrity constraints. It should be used in addition to this section of the
tutorial. 


3.5 Definig Queries
===================

In ConceptBase queries are represented as classes, whose instances 
are the answer objects to the query. The system-internal object 
"QueryClass" may have so-called query classes as instances, which 
contain necessary and sufficient membership conditions for their instances.


Exercise 3.9
Load the object "QueryClass" into the Editor-window.

The syntax of query classes is a class definition with superclasses,
attributes, and a membership condition. The set of possible answers 
to a query is restricted to the set of common instances of all its 
superclasses.

The following query computes all managers, which are bosses of an 
employee:

QueryClass AllBosses isA Manager with
    constraint
	all_bosse_srule:
		$ exists e/Employee
		  (e boss this) $	
end 


The predefined variable "this" in the constraint is identified
with all solutions of the query class, i.e. the solutions 
are substituted for "this" if they satisfy the constraint.

Enter this query into the Editor window and press "Ask" (not "Tell"). 
The query will be evaluated by the server and after a few seconds
the answer will appear both in the Protocoll- and in the Editor-window
If an error has occured and the query was typed correctly, load the 
Employee-frame and check if the frame contains the 
"BossRule", defined in chapter 3.3.
If the answer was correct we can tell the query class "AllBosses" to the 
object base.

The next query uses the query class "AllBosses" to restrict the range of
the answer set:

QueryClass BossesWithSalaries isA AllBosses with
retrieved_attribute 
	salary : Integer
end 

Before this Query can be evaluated "AllBosses" must be told, because
it is referenced in "BossesWithSalaries".

This query returns the instances of AllBosses together with their salaries.
Attributes of the category "retrieved_attribute" must be attributes of one 
of the superclasses of the query class. In this example BossesSalary is a 
subclass of AllBosses, which is subclass of Manager, which is a subclass of 
Employee (uuuff !) and the Employee class contains the attribute "salary". 
So the retrieved_attribute is legal.

Exercise 3.10
Add the query class "BossesWithSalaries" to the object 
base.

Query classes can also contain so called "computed_attributes". These 
attributes are defined for the query class itself, but not for any 
of its superclasses. They are called "computed", because their computation is 
determined by the constraint at runtime. Computed_attributes don't exist 
persistently in the object base, that's why the answers don't get a label.


Example:
A query class computing for all managers the department
he leads.

QueryClass BossesAndDepartments isA Manager with
computed_attribute
	head_of : Department
constraint
	head_of_rule:
	$ (head_of head this) $
end


Exercise 3.11
Define a query class BossesAndEmployees, which is a subclass
of Manager and will return all leaders of departments with their
department and the employees who work there.

More information about query classes can be found in the User manual, 
chapter 2.3 and in the example directory QUERIES.

This was the last Chapter of the tutorial. We hope that it provided a first
impression about ConceptBase and Telos. Refer to the other examples,
especially to RULES+CONSTRAINTS and QUERIES and of course
to the User Manual to learn more about the features of ConceptBase.

Any comments and suggestions concerning this Tutorial or ConceptBase
at all are very welcome and can be posted via email to:

CB@picasso.informatik.rwth-aachen.de

|----------------------|
| The ConceptBase Team |
|----------------------|


4. Solutions to the Exercises
=============================

2.1b)
$CB_HOME/CB_Exe/KSV_Exe/CBserver -p 5544 -d TutApp

2.2)
Select "Connect CB Server" from the Server Menu.
An interaction window appears, querying the host name and the 
port number of the server you want to connect to. 
Enter the name of the host the server was started on and the 
portnumber specified by the -p parameter, then select "Connect". 

3.1)
Telos frame for Department:
Department in Class 
end 

3.2) 
To load an object from the object base into the Telos Editor, 
select the "frame" button or "Load frame" from the Edit menu/

3.3)
Telos frame for Department
Department in Class with
	attribute
		head: Manager 
end 

3.4) Telos frames

a)
Telos frame for Lloyd:

Lloyd in Manager
end

Phil in Manager
end

Eleonore in Manager
end

Albert in Manager
end

b)

Production in Department with
  head
    head_of_Production : Lloyd
end 



Marketing in Department with
  head
    head_of_Marketing : Phil
end 

Administration in Department with
  head
    head_of_Administration : Eleonore
end 

Research in Department with
  head
    head_of_Research : Albert
end 

c)
Lloyd in Manager with
  salary   
      LloydsSalary : 100000
end 

Phil in Manager with
  salary
    PhilsSalary : 120000
end 

Eleonore in Manager with
  salary
    EleonoresSalary : 20000
end 

Albert in Manager with
  salary
    AlbertsSalary : 110000
end 

3.5)

Michael in Employee with
  dept
    MichaelsDepartment : Production
  salary 
    MichaelsSalary : 30000
end 

Maria in Employee with
  dept
    MariasDepartment : Administration
  salary 
    MariasSalary : 10000
end 

Herbert in Employee with
  dept
    HerbertsDepartment : Marketing
  salary 
    HerbertsSalary : 60000
end 

Edward in Employee with
  dept
    EdwardsDepartment : Research
  salary 
    EdwardsSalary : 50000
end 

3.8)
Employee with
constraint
 	salaryIC: $ forall e/Employee m/Manager x,y/Integer 
	(e boss m) and (e salary x) and (m salary y) ==> (x <= y) $
end 

3.11)
QueryClass BossesAndEmployees isA Manager with	
computed_attribute
	emps : Employee;
	head_of : Department
constraint
	employee_rule:
	$ (head_of head this) and (emps dept head_of ) $
end 
