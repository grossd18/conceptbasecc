\chapter{Predefined Query Classes}
\label{cap:builtin-queries}

This chapter gives an overview of the query classes which
are predefined in a standard ConceptBase installation.
The names of parameters of the queries are set in {\ttfamily typewriter} font.
Most of the queries listed here are used by the ConceptBase user 
interface CBIva to interaction with the CBserver. A normal user 
typically formulates queries herself. In fact, most queries listed below are 
very simple and directly representation as query class.
An exception are the functions for computation and counting. They
cannot be expressed by simple query classes but extend the expressiveness
of the system.


\section{Query classes and generic query classes}

These queries can also be used in the constraints of other queries.

\subsection{Instances and classes}

\begin{description}
\item[ISINSTANCE:] Checks whether {\ttfamily obj} is instance of {\ttfamily class}.
The result is either {\ttfamily TRUE} or {\ttfamily FALSE}.
\item[IS\_EXPLICIT\_INSTANCE:] Same as before, but returns {\ttfamily TRUE} only if
{\ttfamily obj} is an {\em explicit} instance of {\ttfamily class}.
\item[find\_classes:] Lists all objects of which {\ttfamily objname} is an instance.
\item[find\_instances:] Lists all instances (implicit and explicit) of {\ttfamily class}.
\item[find\_explicit\_instances:] Same as before, but only explicit instances are returned.
\end{description}

\subsection{Specializations and generalizations}

\begin{description}
\item[ISSUBCLASS:] Checks whether {\ttfamily sub} is subclass of {\ttfamily super}.
The result is either {\ttfamily TRUE} or {\ttfamily FALSE}.
\item[IS\_EXPLICIT\_SUBCLASS:] Same as before, but returns {\ttfamily TRUE} only if
{\ttfamily sub} is an {\em explicit} subclass of {\ttfamily super}.
\item[find\_specializations:] Lists the subclasses of {\ttfamily class}. If {\ttfamily ded}
is {\ttfamily TRUE}, then the result will also include implicit subclasses, if
{\ttfamily ded} is {\ttfamily FALSE} only explicit information will be included.
\item[find\_specializations:] Same as before, but for super classes.
\end{description}

\subsection{Attributes}

\begin{description}
\item[IS\_ATTRIBUTE\_OF:] Returns {\ttfamily TRUE} if {\ttfamily src} has an attribute
of the category {\ttfamily attrCat} which has the value {\ttfamily dst}.
\item[IS\_EXPLICIT\_ATTRIBUTE\_OF:] Same as before, but only for explicit attributes.
\item[find\_all\_explicit\_attribute\_values:] Lists all attribute values of {\ttfamily objname}
that are explicitely defined.
\item[find\_iattributes:] Lists the attributes that {\em go into} {\ttfamily class}.
\item[find\_referring\_objects:] Lists the objects that have an explicit attribute link to {\ttfamily class}.
\item[find\_referring\_objects2:] Lists the objects that have an explicit attribute link
to {\ttfamily objname} and for which the attribute link is an instance of {\ttfamily cat}.
\item[find\_all\_referring\_objects2:] Same as before, but including implicit attributes.
\item[find\_attribute\_categories:] Lists all the attribute categories that may be used for
{\ttfamily objname}. This is a lookup of all attributes of all classes of {\ttfamily objname}.
\item[find\_incoming\_attribute\_categories:] In contrast to the previous query, this query returns
all attribute categories that go into {\ttfamily objname} (i.e.\ attribute categories for which
{\ttfamily objname} can be used as an attribute value).
\item[find\_attribute\_values:] Lists all objects that are attribute values of {\ttfamily objname}
in the attribute category {\ttfamily cat}.
\item[find\_explicit\_attribute\_values:] Same as before, but only for explicit attributes.
\end{description}

\subsection{Links between objects}

\begin{description}
\item[find\_incoming\_links:] Lists the links that {\em go into} {\ttfamily objname} and are instance
of {\ttfamily category}. Note that all types of links are returned, including attributes,
instance-of-links and specialization links.
\item[find\_incoming\_links\_simple:] Same as before, but without the parameter {\ttfamily category}.
\item[find\_outgoing\_links:] Lists the links that {\em come out of} {\ttfamily objname} and are instance
of {\ttfamily category}. Note that all types of links are returned, including attributes,
instance-of-links and specialization links.
\item[find\_outgoing\_links\_simple:] Same as before, but without the parameter {\ttfamily category}.
\item[get\_links2:] Return the links between {\ttfamily src} and {\ttfamily dst}.
\item[get\_links3:] Return the links between {\ttfamily src} and {\ttfamily dst} that are
instance of {\ttfamily cat}.
\end{description}

\subsection{Other queries}

\begin{description}
\item[find\_object:] This query just returns the object given as parameter {\ttfamily objname},
if it exists. Thus it can be used to check whether {\ttfamily objname} exists, but
there is a builtin query {\ttfamily exists} which does the same. The query is
mainly useful in combination with a user-defined answer format (e.g.\ the Graph Editor
is using this query to retrieve the graphical representation of the object).
\item[AvailableVersions:] Lists the instances of {\ttfamily Version} with
the time since when they are know. This query is used by the user interface
to use a different rollback time (Options $\rightarrow$ Select Version).
\item[listModule:] Lists the the content of a module as Telos frames, see
also section \ref{sec:module_content}.
\item[listModuleReloadable:] Like listModule but adding a flag to set the module context to the right module context when the frames need to be loaded again into a CBserver.
\end{description}


\section{Functions}
\label{cap:builtin-queries:functions}

Functions may also be used within other queries. You may define your own functions
(see section \ref{sec:functions}).

\subsection{Computation and counting}
\begin{description}
\item[COUNT:] counts the instances of a class, this may be also a query class
\item[SUM:] computes the sum of the instances of a class (must be reals or integers)
\item[AVG:] computes the average of the instances of a class (must be reals or integers)
\item[MAX:] gives the maximum of the instances of a class (wrt. the order of $<$ and $>$, see section \ref{sec:CBL})
\item[MIN:] gives the minimum of the instances of a class  (wrt. the order of $<$ and $>$, see section \ref{sec:CBL})
\item[COUNT\_Attribute:] counts the attributes in the specified category of an object
\item[SUM\_Attribute:] computes the sum of the attributes in the specified category of an object (must be reals or integers)
\item[AVG\_Attribute:] computes the average of the attributes in the specified category of an object (must be reals or integers)
\item[MAX\_Attribute:] gives the maximum of the attributes in the specified category of an object (wrt. the order of $<$ and $>$, see section \ref{sec:CBL})
\item[MIN\_Attribute:] gives the minimum of the attributes in the specified category of an object (wrt. the order of $<$ and $>$, see section \ref{sec:CBL})
\item[PLUS:] computes the sum of two reals or integers
\item[MINUS:] computes the difference of two reals or integers
\item[MULT:] computes the product of two reals or integers
\item[DIV:] computes the quotient of two reals or integers
\item[IPLUS:] computes the sum of two integers; result is an integer number
\item[IMINUS:] computes the difference of two integers; result is an integer number
\item[IMULT:] computes the product of two integers; result is an integer number
\item[IDIV:] computes the quotient of two integers and then truncates the quotient to the
largest integer number smaller than or equal to the quotient
\end{description}

ConceptBase realizes the arithmetic functions via its host Prolog system SWI-Prolog. 
Integer numbers on Linux are represented 
as 64-bit numbers, yielding a maximum range from $-2^{64}-1$
to $2^{64}-1$. SWI-Prolog supports by default under           
Windows and Linux64 arbitrarily long integers.
Real numbers are implemented by SWI-Prolog as 64-bit double precision
floating point numbers. ConceptBase uses 12 decimal digits.

\subsection{String manipulation}
\begin{description}
\item[ConcatenateStrings:] concatenates two labels, typically strings; the two arguments
  can be expressions that are concatenated after their evaluation
\item[ConcatenateStrings3:] concatenates three labels; arguments may not be expressions
\item[ConcatenateStrings4:] concatenates four labels; arguments may not be expressions
\item[concat:] same as ConcatenateStrings
\item[StringToLabel:] removes the quotes of a string and returns it as a label (not
an object), useful if labels should be passed as a parameter of a query
\item[toLabel:] evaluates the argument and that creates an individual object with
  the canonical representation of the argument result; the canonical representation is
  an alphanumeric where special characters are replaces by substrings like {\tt "C30\_"}
  for special characters
\end{description}

For example, the expression {\tt toLabel(concat("*",1+2))} will return
{\tt C42\_3}. The substring {\tt "C42\_"} is the canonical representation (ASCII number)
of {\tt "*"}. The subexpression {\tt 1+2}  is evaluated to 3.

\section{Builtin query classes}

These queries must not be used within other queries as they do not return
a list of objects. They may only be used directly from client programs.

\begin{description}
\item[exists:] Checks whether {\ttfamily objname} exists and returns {\ttfamily yes} or {\ttfamily no}.
\item[get\_object:] Returns the frame representation of {\ttfamily objname}. This
query may be either called with just one parameter ({\ttfamily objname}) or with
four parameters ({\ttfamily objname}, {\ttfamily dedIn}, {\ttfamily dedIsa}, {\ttfamily dedWith}).
The {\ttfamily ded*}-parameters are boolean flags that indicate whether
implicit (deduced) information should also be included in the frame representation.
Note that the order of the parameters hast to be the same as listed above.
\item[get\_object\_star:] Returns the frame representation for all objects with a label
that match the given wildcard expression. Only simple wildcards with a star (*) at the end
are allowed.
\item[rename:] Renames an object from {\ttfamily oldname} to {\ttfamily newname}.
This is a low-level operation directly on the symbol table that works directly
on the symbol table. It only checks whether {\em newname} is not already used as
label for a different object, no other consistency checks are performed.
The parameters have to be given in the order {\ttfamily newname}, {\ttfamily oldname}.
\end{description}


\chapter{CBserver Plug-Ins}
\label{cap:lpi}

An LPI plug-in ("logic plug-in") is a small Prolog program that is attached
to the CBserver (which is implemented in Prolog) at startup-time.
It extends the functionality of the CBserver, 
for example for user-defined builtin queries. A plug-in can also interface to
the services of the operating system%
\footnote{Since we currently supply the CBserver for Linux only, you need to run the CBserver
on a local Linux computer in your network if you want to use the plug-ins.
Note that we supply a ready-to-use
virtual appliance that includes Linux and ConceptBase and that can be executed
via a virtualization engine, see
\url{http://conceptbase.sourceforge.net/import-cb-appliance.html}
for details.}.


You can create a file like {\tt myplugin.swi.lpi} to provide the implementation 
for user-defined builtin queries or for call actions in 
active rules (see section \ref{sec:eca}). You can use the full range of functions
provided by the underlying Prolog system 
(here: SWI-Prolog, \url{http://www.swi-prolog.org}) and the functions of the CBserver to realize 
your implementation. You can consult the the CB-Forum for some examples at
\url{http://merkur.informatik.rwth-aachen.de/pub/bscw.cgi/2768063}.
You find there examples for sending emails 
from active rules, and for extending the set of builtin queries and functions.

\section{Defining the plug-in}
Once you have coded your file {\tt myplugin.swi.lpi},
there are two methods to attach it to the CBserver. The first method is to copy the file into an
existing database directory created by the CBserver:

\begin{verbatim}
  cbserver -g exit -d MYDB 
  cp myplugin.swi.lpi MYDB
\end{verbatim}

The first command creates the database directory MYDB if not already existing and initializes
it with the pre-defined system classes and objects. The second command copies
the LPI file to the database directory. 
This method makes the definitions only visible to a CBserver that loads the database MYDB.

The second method instructs ConceptBase to load your LPI code to {\em any\/} new database
created by the CBserver. To do so, copy the LPI file into the system database directory that
holds the definitions of predefined ConceptBase objects:

\begin{verbatim}
  cp myplugin.swi.lpi <CB_HOME>/lib/SystemDB
\end{verbatim}

where {\tt CB\_HOME} is the directory into which you installed ConceptBase.
The number of LPI files is not limited. You may define zero, one or any number
of plug-in files.

A couple of useful LPI plug-ins are published via the CB-Forum,
see \url{http://merkur.informatik.rwth-aachen.de/pub/bscw.cgi/2768063}.
Note that these plug-ins are copyrighted and typically come with their own
license conditions that may be different to the license conditions
of ConceptBase. If you plan to use the plugins for commercial purposes,
you may have to acquire appropriate licenses from the plug-in's authors.


\section{Calling the plug-in}
There are two ways to trigger the call of a procedure implemented by an LPI plugin.

\begin{enumerate}
\item By explicitely calling a builtin query class (or function) whose code has been implemented by the LPI plugin.
The LPI code would then look similar to the code in SYSTEM.SWI.builtin and you must have
defined an instance of {\tt BuiltinQueryClass} that matches the signature of the
LPI code. The call to the builtin query class may be enacted from the user interface, or
it may be included as an {\tt ASKquery} call in an instance of {\tt AnswerFormat}. 
Refer for more information to section \ref{subsec:externalproc} and to the directory
{\tt Examples/BuiltinQueries} in your ConceptBase installation directory.
\item By calling the implemented function as a CALL action of an active rule. See section \ref{subsec:counter}
for an example. In that case, there does not need to be a definition of a builtin query
class (or function) to declare the signature of the procedure.
\end{enumerate}

If the code of an LPI plugin realizes a ConceptBase function, e.g. selecting the first instance of a class,
then you can use that function whereever functions are allowed. As an example,
consider the definition of the LPI plugin {\tt selectfirst.swi.lpi} from 
\url{http://merkur.informatik.rwth-aachen.de/pub/bscw.cgi/d2984654/selectfirst.swi.lpi.txt}:

\begin{verbatim}
  compute_selectfirst(_res,_class,_c1) :-
        nonvar(_class),                              
        cbserver:ask([In(_res,_class)]),             
        !.

  tell: 'selectfirst in Function isA Proposition with
  parameter class: Proposition end'.
\end{verbatim}


The first clause is the Prolog code. The predicate must start with the prefix {\tt compute\_} followed by the
name of the function.
The first argument is for the result of the function. Subsequently, each input parameter is
represented by two arguments, one for the input parameter itself and a second as a placeholder
of the type of the input parameter. 
The second clause tells the new function as ConceptBase object so that it can be used like any other ConceptBase
function. For technical reasons, the 'tell' clause may not span over more than 5 lines. Use
long lines if the object to be defined is large. The function object (here: {\tt selectfirst}) is stored
in the {\tt System} module of the database. This is the root module of the module hierarchy, thus
functions defined in this way are visible and executable in all sub-modules.
If you omit the 'tell' clause in the LPI file, then you need to tell it manually to the
database. This can also be done in a module different to {\tt System}. In this case, the
function can only be called in those modules where the function object is visible.

If you just want to invoke the procedure defined in an LPI plugin via the CALL clause of an active rule,
you  do not need to include a 'tell' clause. Consider for example the SENDMAIL plugin from
\url{http://merkur.informatik.rwth-aachen.de/pub/bscw.cgi/2269675}.

You need to be very careful with testing your code. Only use this feature for functions
that cannot be realized by a regular query class or by active rules. LPI code has the 
full expressiveness of a programming language. Program errors may lead to crashes of the CBserver
or to infinite loops or to harmful behavior such as deletion of files. 
Query classes, deductive rules and integrity constraints can never loop infinitely 
and can (to the best of our knowledge) only produce answer, not changes to your file system
or interact with the operating system.
Active rules could loop infinitely but also shall not change your file system and shall not 
interact with the operating system unless you call such code explicitely in the active rule.

You can disable the loading of LPI plugins with the CBserver option {\tt -g nolpi}. The
CBserver will then not load LPI plugins upon startup. This option might be useful for
debugging or for disabling loading LPI plugins that are configured in the {\tt lib/SystemDB}
sub-directory of your ConceptBase installation.



\section{Programming interface for the plug-ins}
\label{sec:cbserver-extensions}
The CBserver plug-ins need to interface to the functionalities of the CBserver, the Prolog
runtime, and possibly the operating system. To simplify the programming of the CBserver
plug-ins, we document here the interface of the module {\tt cbserver}. We assume
that the code for the plug-ins is written in SWI-Prolog and the user is familiar with
the SWI-Prolog system.

\begin{description}

\item[cbserver:ask(Q,Params,A)] asks the query {\tt Q} with 
parameters {\tt Params} to the CBserver. The answer is returned as
a Prolog atom in {\tt A}. The atom holds the answer represented
in the answer format of {\tt Q}, see also chapter \ref{sec:answerformat}. \newline
Example: {\tt cbserver:ask(find\_classes,[bill/objname],A)}

\item[cbserver:ask(Preds)] evaluates the predicates in list {\tt Preds}. The 
predicate can backtrack and will bind in case of success the free variables
in {\tt Preds}. We currently support only the following predicates: {\tt In}, 
{\tt A}, {\tt AL}, and {\tt Isa}.\newline
Example: {\tt cbserver:ask([In(X,Employee),A(X,salary,1000)])}

\item[cbserver:askAll(X,Preds,Set)] finds all objects {\tt X} that satisfy
the condition in {\tt Preds} and puts the result into the list {\tt Set}. 
Supported predicates in {\tt Preds} are:  {\tt In}, {\tt A}, {\tt AL}, and {\tt Isa}.\newline
Example: {\tt cbserver:askAll(X,[In(X,Employee),A(X,salary,1000],S)}

\item[cbserver:tellFrames(F)] tells all frames contained in the atom {\tt F}.
The call will fail if there is any error in {\tt F}.\newline
Example: {\tt cbserver:tellFrames('bill in Employee end')}

\item[cbserver:makeName(Id,A)] converts an object identifier to a readable
object name.

\item[cbserver:makeId(A,Id)] converts an object name (Prolog atom) into
an object identifier used by the CBserver to identify Telos objects.
If {\tt A} is already an object identifier, it is returned as well
in {\tt Id}.

\item[cbserver:arg2val(E,V)] transforms an argument (either an object identifier
or a functional expression) to a value (a number or a string). The value can then
be used  in Prolog style computations such as arithmetic expressions.

\item[cbserver:val2arg(V,I)] transforms a Prolog value (number, string) to an
object identifier, possibly by creating a new object for the value. 

\item[cbserver:concat(X,Y)] concats the strings (Prolog atoms) contained
in the list {\tt X}. The result is returned in {\tt Y}.

\end{description}

Note that ConceptBase internally manages concepts by their object identifier. The
programming interface instead addresses concepts (and objects) by their name, i.e.\ 
the label of the object or the Prolog value corresponding to the label.
You may have to use the procedure {\tt makeName} and {\tt makeId} to switch between
the two representations. The two 
procedures {\tt arg2val} and {\tt val2arg} are useful for defining new builtin
functions on the basis of Prolog's arithmetic functions.
Assume for example, that the object identifier {\tt id123} has been created to correspond to
the real number {\tt 1.5}. Then, the following relations hold: {\tt makeName(id123,'1.5')},
{\tt arg2val(id123,}$1.5${\tt)}. Hence, {\tt makeName} returns the label {\tt '1.5'} whereas
{\tt arg2val} returns the number $1.5$.

The interface shall be extended in the future to provide more functionality.
Be sure that you only use this feature if user-defined query classes cannot
realize your requirements!






