

\chapter{Introduction}
\label{cha:Introduction}

ConceptBase.cc is a deductive object base management system for meta databases.
Its data model is a conceptual modeling language making it particularily
well-suited for design and modeling applications. Its underlying data model
allows to uniformly represent data, classes, meta classes, meta meta classes
etc. yielding a powerful metamodeling environment.
The system has been used in
projects ranging from development support for data-intensive applications
\cite{DWQIS99}, requirements engineering \cite{rd92,EberleinDiss,USU96},
electronic commerce \cite{MEMO}, and version\&configuration
management \cite{rjg91} to co-authoring of technical documents \cite{hjek90}.
It has mostly been used in academia for developing specialized modeling
languages by means of metamodeling \cite{Emisa,jjm09,jeus09}.

The key features distinguishing ConceptBase.cc from other extended DBMS and
meta-modeling systems are:
\begin{itemize}
\item Unlimited meta class hierarchy, allowing to represent information at any
      abstraction level (data, class, meta class, meta meta class)
\item Uniform data structure (called {\em P-fact}) for concepts,
      their attributes, their class memberships, and their super- and
      sub-concepts; all four types of information are full-fledged objects
\item Clean formal integration of deductive and object-oriented abstraction by
      Datalog logical theories
\item Complex computations can be user-defined by recursive function definitions, e.g.
      the length of the shortest path between two nodes
\item Queries are defined as classes with user-defined membership constraints;
      queries can range over any type of object at any abstraction level
\item Active rules can be used to define the system's reaction to events; active
      rules can change the state of the database and can trigger each other
\item Client-server architecture with wide-area Internet access
\end{itemize}


ConceptBase.cc implements the version O-Telos (= Object-Telos)
of the knowledge representation language
Telos \cite{mbjk90}. O-Telos integrates a thoroughly axiomatized structurally
object-oriented kernel with a predicative assertion language in the style
of deductive databases. A complete formal definition can be found in
\cite{JIIS,jeus92}. O-Telos is purely based on deductive logic but it also
supports a frame-like representation of facts.

This user manual is tightly integrated with the ConceptBase.cc Forum. The ConceptBase.cc Forum is
an Internet-based workspace where ConceptBase.cc developers and users share knowledge
and example models.
It contains numerous examples on how to solve certain modeling problems. It is
highly recommended to join the workspace.
More details are available at
\url{http://conceptbase.sourceforge.net/CB-Forum.html}.

ConceptBase.cc is mainly used for metamodeling and for engineering customized
modeling languages. The textbook \cite{jjm09}

{\small
\begin{verbatim}
     Jeusfeld, M.A., M. Jarke, and J. Mylopoulos:
     Metamodeling for Method Engineering.
     Cambridge, MA, 2009. The MIT Press, ISBN-10: 0-262-10108-4.
\end{verbatim}
}

introduces into the topic and provides six in-depth case studies ranging from 
requirements engineering to chemical device modeling. The book and this
user manual are complementary to each other.



\section{Background: Telos and O-Telos}

The knowledge representation language {\em Telos} has been
one of the earliest attempts to integrate deduction, object-orientation
and metamodeling \cite{STAN86,mbjk90}.
The O-Telos \cite{jeus92} dialect supported in ConceptBase.cc has as design goals the
semantic simplicity, symmetry of deductive and object-oriented views, metamodeling flexibility,
and extensibility at any abstraction level.
This emphasis, technically supported by a careful mapping of O-Telos to Datalog
with negation (DATALOG$^\neg$), has paid off both in user acceptance
and ease of implementation. In essence, ConceptBase.cc is based on deductive database
technology with object-oriented abstraction principles like object identity,
class membership, specialization and attribution being coded as pre-defined
deductive rules and integrity constraints.

Development of ConceptBase.cc started in late 1987 in the context of
ESPRIT project DAIDA \cite{JARK93} and was continued within
ESPRIT Basic Research Actions Compulog 1 and 2 (1989 -- 1995), the
ESPRIT LTR project DWQ (Foundations of Data Warehouse Quality, 1996-1999),
and the ESPRIT project MEMO (Mediating and Monitoring Electronic Commerce, 1999-2001).
Versions have been distributed for research experiments since early 1988.
ConceptBase.cc has been installed at more
than eight hundred sites worldwide and is seriously used by about a dozen research projects in
Europe, Asia, and the Americas.

The direct predecessor of O-Telos is the knowledge representation language Telos (specified by 
John Mylopoulos, Alex Borgida, Martin Stanley, Manolis Koubarakis, Dimitris Plexousakis, and others). 
Telos was designed to represent information about information systems, esp. requirements. 
Telos was based on CML (Conceptual Modeling Language) developed in the mid/late 1980-ties. 
A variant of CML was created under the label SML (System Modeling Language)\footnote{
   The acronym SML survived in ConceptBase as the filetype {\tt .sml} for source models.
}
and 
implemented by John Gallagher and Levi Solomon at SCS Hamburg. CML itself was based on 
RML (Requirements Modeling Language) developed at the University of Toronto by Sol Greenspan and others.
Neither RML nor CML were implemented in 1987. They were regarded as theoretic 
knowledge representation languages with 'possible world semantics'. 
SML was implemented as a subset of CML using Prolog's SLDNF semantics. 

In 1987, we decided to start an implementation of Telos and quickly realized that the original semantics 
was too complex for an efficient implementation. The temporal component of Telos included 
both valid time (when an information is true in the domain) and transaction time 
(when the information is regarded to part of the knowledge base). The temporal reasoner for the 
dimensions in ConceptBase.cc V3.0 only to see that there were undesired effects with the query evaluator 
and the uniform representation of information into objects. Specifically, the specialization of a 
class into a subclass could have a valid time which could be incomparable to the valid time of an 
instance of the subclass. Any change in the network of valid time intervals could change the 
set of instances of a class. Because of that, we dropped the valid time as a built-in feature of 
objects but we kept the transaction time. A few other features like the declarative TELL, UNTELL 
and RETELL operations as proposed by Manolis Koubarakis in his master thesis on Telos were 
only implemented in a rather limited way - essentially forbidding direct updates to derived facts. 
On the other hand, O-Telos extends the universal object representation to any piece of explicit 
information and reduces the number of essential builtin objects to just five. 
So, some of the roots of Telos in artificial intelligence were abandoned in favor 
of a clear semantics and of better capabilities for metamodeling.
More specifically, there are some important differences of O-Telos to the original Telos specification.

\begin{itemize}
\item Labels are not used as object identfiers in O-Telos. For example, Telos would represent an
object like "bill" as P(bill,bill,-,bill). In O-Telos, we represent such objects as
P(id123,id123,bill,id123). Each object in O-Telos has a system-generated identifier.
\item Telos promotes the use of level classes such as "Token", "SimpleClass", "MetaClass" etc. to
classify user-defined objects into abstraction levels similar to the OMG UML Infrastructure. Besides,
Telos defines a so-called omega-level that is parallel to the abstraction levels and defines
in particular the object "Proposition". O-Telos does not pre-define the level classes and does not
need them. It is up to the user to demand the existence of such levels.
\item There are only 5 pre-defined "omega" objects in Telos to define "individual" objects, instantiations,
specializations, attributions, and the most-generic object "Proposition". Telos has some more
predefined such objects like "Class" and "AttributeClass". Such objects are regarded in O-Telos as
user-definable objects.
\item There are about 50\% more builtin axioms in O-Telos (see appendix \ref{cha:otelos-axioms})
compared to Telos, in particular to define
attribute specialization and to forbid certain pathological databases with groups of entangled
objects.
\end{itemize}

The ConceptBase.cc server adds further capabilities to O-Telos that were not defined/implemented
in Telos:

\begin{itemize}
\item There are deductive rules and integrity constraints that are mapped to DATALOG$^\neg$ and evaluated
by a DATALOG$^\neg$ engine.  The engine supports stratified negation, checked at runtime. 
\item There is a query language that is embedded into the notion of class specialization and
realized by the DATALOG$^\neg$ engine. Query classes work like classes whose instances are derived by 
deductive rules.
\item There are active rules that allow to trigger actions when certain events occur.
\item Arithmetic is supported as well as recursive function definitions. The implementation utilizes
the DATALOG$^\neg$ engine for evaluating recursive function expressions.
\item ConceptBase provides a partial evaluator for rules and constraints that range over 
multiple instantiation levels. Example of such formulas were also proposed for Telos, in particular
for cardinality constraints. The partial evaluator makes sure that the DATALOG$^\neg$ engine can always
detect stratification violations.
\item ConceptBase.cc provides an elaborate module concept for O-Telos databases. Sub-modules
see all definitions from super-modules but not of sibling modules (unless imported).
\item Stratification in ConceptBase.cc is solely defined in terms of logical stratification
(recursive rules in the presence of negation). There is no need on ConceptBase.cc to demand fixed
abstraction levels to constrain the instantiation relation between objects. Since ConceptBase.cc
is mostly used for metamodeling, the user can define such constraints if they are necessary for
the modeling domain.
\end{itemize}



Since we wanted to be able to manage large knowledge bases (millions of concepts rather than a few hundred), 
we decided to select a semantics that allowed efficient query evaluation. Telos included already features 
for deductive rules and integrity constraints. Thus, the natural choice was DATALOG$^\neg$ 
with perfect model semantics. The deductive rule evaluator and the integrity checker 
were ready in 1990. A query language ("query classes") followed shortly later. 
O-Telos exhibits an extreme usage of DATALOG$^\neg$:

\begin{itemize}
\item There is only one base relation P(o,x,l,y) called P-facts used for all objects, classes, meta classes, 
attributes, class membership relationships, and specialization relationships as well as for 
deductive rules, integrity constraints and queries.
\item The semantics of class membership, specialization, and attribution is encoded by around 30 axioms, 
which are either deductive rules or integrity constraints.
\item Deductive rules ranging over more than one classification level (instances, classes, meta 
classes, etc.) are partially evaluated to a collection of rules (or constraints) ranging over 
exactly one classification level.
\end{itemize}

O-Telos should be regarded as the data model of a database for metamodeling. It is capable to 
represent semantic features of (data) modeling languages like entity-relationship diagrams and 
data flow diagrams. Once modeled as meta classes in O-Telos, one simply has to tell the meta 
classes to ConceptBase.cc to get an environment where one can manipulate models in these modeling languages. 
Since all abstraction levels are supported, the models themselves can be represented in 
O-Telos (and thus be managed by ConceptBase.cc).






\section{The architecture of ConceptBase.cc}
\label{sec:architecture}

ConceptBase.cc \relnr\ follows a client-server architecture. Clients and servers
run as independent processes which interact via inter-process communication
(IPC) channels (Fig. 1-1).  Although this communication channel was
initially meant for use in local area networks, it has been used
successfully for nationwide and even transatlantic collaboration of clients
on a common server.

The ConceptBase.cc server (CBserver) offers programming interfaces that allow to build
clients and to exchange messages in particular for updating and querying
object bases using the Telos syntax. We provide support for Java and to a very limited
degree for C/C++. 
Descriptions of the interfaces and the corresponding
libraries that are delivered with ConceptBase.cc can be found in the {\bfseries
ConceptBase.cc Programmers Manual}, available via the CB-Forum at
\url{http://merkur.informatik.rwth-aachen.de/pub/bscw.cgi/885553}.
We like to note that the C/C++ interfaces
were not maintained since we switched the user interface to Java. 

Besides the Java/C API,
the CBShell client (see section \ref{cha:cbshell}) can be used to interact with a CBserver
via the command line or in shell scripts. CBShell is indeed a Java client of the CBserver.
The CBShell can also serve as an example client for programming own application
specific client tools.
There is also a tool that creates an
HTTP interface to a CBserver, see section BrokerReproxy in the CB-Forum 
\url{http://merkur.informatik.rwth-aachen.de/pub/bscw.cgi/895647}. Clients would then
interact with ConceptBase via HTTP requests.


\cbfigure{cb_fig1}{8.5cm}{The client-server architecture of ConceptBase.cc}


ConceptBase.cc comes with a standard usage environment
implemented in Java which supports editing, ad-hoc querying
and browsing (CBIva). The tool CBGraph supports editing diagrams extracted from
the database, and CBShell is a command line shell for interacting with the database.

Although ConceptBase.cc provides multi-user support and
an arbitrary number of clients may be connected to the same server process,
ConceptBase.cc does not yet support concurrency control beyond a forced
serialization of messages.

A performance comparison \cite{Lud2010} of ConceptBase.cc with Proteg{\'e}/Racer found that 
ConceptBase.cc is orders of magnitude faster for queries. It lacks however the
reasoning capabilities of Proteg{\'e}/Racer.



\section{Hardware and software requirements}
\label{sec:requirements}

The ConceptBase.cc server (CBserver) can be compiled on at
least the following platforms\footnote{All trademarks are property of their respective
owners.} including
\begin{itemize}
\item i386 CPUs under Linux Kernel 3.0 or higher,
\item x86\_64 (AMD64) CPUs under Linux Kernel 3.0 or higher,
\item x86\_64 (AMD64) CPUs under Windows 10 (Creators Update or later) with Linux sub-system enabled.
\item ARMv7 (ARM) CPUs under Raspbian (Raspberry Pi)
\end{itemize}
Pre-compiled binaries are provided for Linux (and thus also Windows 10).
Compilation from the ConceptBase.cc sources on Mac OS-X and other platforms should
be possible in principle, though we cannot provide support for them.
See instructions distributed with the ConceptBase.cc source files for further details.

Implementation languages for the CBserver are Prolog\footnote{ConceptBase.cc now
relies on SWI-Prolog [\url{http://www.swi-prolog.org/}]. Formerly,
ProLog by BIM had been used. We only use constructs of SWI-Prolog 5.6 but
later versions, in particular SWI-Prolog 6.x, should be compatible.}
(in particular for logic-based transformation and compilation
tasks) and C/C++ (in particular for persistent object
storage and retrieval).

The ConceptBase.cc usage environment (CBIva, CBGraph, CBShell) executes on any platform with
a compatible Java Virtual Machine. Java 6, Java 7, or Java 8
should all work. We recommend the most recent stable version of Java 7 or
Java 8.

The CBserver is dynamically linked with a couple of shared
libraries. Under Linux/Unix can check whether all required libraries
are installed by
  
\begin{verbatim}
export PATH=$CB_HOME/bin:$PATH
ldd $CB_HOME/`CBvariant`/bin/CBserver
\end{verbatim}



The installation of ConceptBase.cc requires about 50 MB of free hard disk space. The
main memory requirements depend on the size of the object base
loaded to the ConceptBase.cc server. The initial main memory footprint is just
about 8 MB. We recommend about 20 MB of free main memory for
small applications and 200 MB and higher for large applications
of ConceptBase.cc. The server can handle relatively large databases
consisting of a few million objects.  Response times depend on
the size of the database and even more on the structure of the query.

Since clients connect to a CBserver via Internet, the 
server requires the TCP/IP protocol to be available on both the client and
the server machine (can be the same computer for single-user scenarios).
Note that a firewall installed on the path between the client and
the server machine might block remote access to a CBserver.
The default port number used for the communication between server and
client is 4001. It can be set to another port number by a command line
parameter.

The CBserver is by default multi-user capable, i.e. multiple clients
can connect to the same CBserver. This feature is by default disabled
when you start the CBserver from within the user interface. See
section \ref{cha:cbserver} for more details.

The standard ConceptBase.cc client are CBIva and CBGraph (see section \ref{cha:workbench}).
The distribution also contains a client CBShell that can be used to
interact with a ConceptBase.cc server using a command/shell window.
The CBShell client can also be used to run non-interactive scripts,
e.g. for loading a sequence of files with Telos source models into
the CBserver.

\subsection{Installation}
\label{sec:installation}

The download and installation instructions are available from the
ConceptBase.cc home page at
 \url{http://conceptbase.sourceforge.net/CB-Download.html}.
The binaries are installed via a self-extracting Java Archive (CBinstaller.jar).

The sources are made available as a ZIP archive CBPOOL.zip. 
Compilation from sources on platforms different from Linux
requires in-depth expertise due to the manifold of programming languages
used for ConceptBase (Prolog, C, C++, Java).

You can also install a virtual appliance (Linux) that includes the binaries, sources,
and the complete development environment.

The default installation directory under Windows is {\tt c:\textbackslash conceptbase}. Under Linux,
ConceptBase is installed by default in the user's home directory
{\tt \$HOME/conceptbase}.



\section{Overview of this manual}
\label{sec:overview}

This manual provides detailed information about using ConceptBase.cc. Information about
the installation procedure can be found in the Installation Guide in
directory doc/TechInfo. New users are advised
to follow the installation guide for
getting the system started and then to work through the ConceptBase.cc
Tutorial. More information about the knowledge representation mechanisms,
the applications, and the implementation concepts can be found in the
references. Chapter \ref{cap:language} describes the ConceptBase.cc version of
the Telos language and gives some examples for its usage. Chapter
\ref{cha:cbserver} discusses the parameters that can be set
when starting the CBserver.
Finally, section
\ref{cha:workbench} describes the ConceptBase.cc Usage Enviroment.

Appendices contain a formal definition of the Telos syntax and internal data
structures (\ref{cha:syntax}). Appendix \ref{cha:graph-typen} summarizes
the mechanism for assigning graphical types to objects and adapting
the graphical browsing tool for specific application needs. Appendix
\ref{cap:examples} contains the full Telos notation of an example model
(\ref{sec:employee-model}) and a case study on the modeling of
entity-relationship diagrams with Telos (\ref{sec:ER-diagrams}).
Plenty of further examples for particular application domains and add-ons for
metamodeling can be retrieved from the ConceptBase.cc Forum at
\url{http://conceptbase.sourceforge.net/CB-Forum.html}. 

\section{Differences to earlier versions}
\label{sec:differences}

ConceptBase.cc \relnr\ should be largely source-compatible to its direct predecessor.
The binary database files and the graph files have a new format and
are not compatible with their counterparts created by earlier releases.

The CBserver has now the ability to maintain module sources and
query results formatted in external formats in the file system. 
The module sources allow to co-develop models both via the ConceptBase.cc
user interface and by external text editors. Exporting query results in
external formats is useful when they are post-processed by external
tools. For example, one can generate program source code from 
a ConceptBase.cc model and have that code processed by a compiler.

The active rule component now supports constructs to enforce a
transactional execution of delayed triggers. Triggers can be passed to different queues
that are processed with different priorities. This feature allows to
delay certain triggers until the consequences of the current trigger are
all processed.

The graph editor is now a stand-alone tool and has the ability to store connection details
plus a snapshot of the module sources in its graph files. The graph files
are then self-contained and can be viewed and updated without having
to maintain the module sources elsewhere. It also can display a background
image in the graph window. Nodes can now be configured to be resizable.
Lines are now drawn with anti-aliasing, yielding much better graph images.

The CBShell utility now behaves more like a Linux/Unix shell. It provides
easy shortcuts like 'cd' for changing the module context. It also
allows the use of positional parameters, hence making it a better
companion to Linux/Unix scripts.

The release notes to ConceptBase.cc \relnr\ lists
all major changes and issues. You find the release notes in the subdirectory
{\tt doc/TechInfo} of your ConceptBase.cc installation directory or via
the web site \url{http://conceptbase.cc}.

The  system still has about the same memory footprint as it used to be 10 years ago. You can
easily install the complete system for all supported platforms on a 32 MB memory
stick.

\newpage
\section{License terms}
\label{sec:license}

ConceptBase.cc is distributed under a FreeBSD-style copyright license since June 2009.
Both binary and source code are available via \url{http://sourceforge.net/projects/conceptbase/files}. 

The FreeBSD-style copyright license of ConceptBase.cc reads like follows:

{\scriptsize
\begin{verbatim}
The ConceptBase.cc Copyright

Copyright 1987-2020 The ConceptBase Team. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice, this list of
      conditions and the following disclaimer.
   2. Redistributions in binary form must reproduce the above copyright notice, this list of
      conditions and the following disclaimer in the documentation and/or other materials
      provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE CONCEPTBASE TEAM ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE CONCEPTBASE TEAM OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those of the authors
and should not be interpreted as representing official policies, either expressed or implied,
of the ConceptBase Team.
\end{verbatim}
}

This license makes ConceptBase.cc free software as promoted by the Free Software Foundation. 
The license is upwards-compatible to the GNU Public License (GPL), i.e.\ developers
can combine GPL-ed software with ConceptBase.cc software as long as they
include the above FreeBSD-style license for the ConceptBase.cc components.
It should also be compatible with many other free license
models. The source code is copyrighted by The ConceptBase Team, consisting of
all contributors to the source code at the central code repository of the system.

Binary and source distributions of ConceptBase.cc may contain third-party software.
Their licenses are listed by default in the directory {\tt doc/ExternalLicenses}
of the installation directory. 


You are welcome to contribute the ConceptBase.cc project! Join the 
ConceptBase.cc Forum at
\url{http://conceptbase.sourceforge.net/CB-Forum.html} to do so.
We are also happy to learn about your research/application, in which ConceptBase.cc plays a role.
If you publish results of your work, then please include a reference pointing to this
user manual and/or to the standard ConceptBase.cc reference \cite{JIIS}. 

The ConceptBase.cc system is published under the liberal FreeBSD-style license, which allows
commercial use and modification of the source code. This user manual is
however published under a less liberal copyright license. Use is permitted for
private and academic purposes. Moreover, commercial users may use the manual to
self-study the ConceptBase.cc system. Only members of The ConceptBase Team,
see \url{http://conceptbase.cc/cbteam.html}, are allowed to modify the user manual.
Re-publication of the user manual in print or online is not permitted.
If you make changes to your own copy of the source code of ConceptBase.cc,
then you may not document them in this user manual (or its sources).
You rather should write a companion report that lists the differences of
your version of ConceptBase.cc to the user manual distributed via the home page of
ConceptBase.cc (\url{http://conceptbase.cc}). 

The ConceptBase.cc Forum (\url{http://conceptbase.sourceforge.net/CB-Forum.html})
contains material submitted by different authors. The default license for
the material on the CB-Forum is "Creative Commons BY-NC 4.0", which does not permit
commercial use of its content. 

The ConceptBase.cc logo at \url{http://conceptbase.sourceforge.net/conceptbase-cc-logo.gif}
is created and copyrighted by Manfred Jeusfeld. It may be used on official
ConceptBase.cc web sites, the ConceptBase.cc User Manual and Tutorials, and the ConceptBase.cc Forum.
It may also be included in binary distributions created by members of the ConceptBase Team.
If you are not a member of the ConceptBase.cc Team and like to use the logo for your own binary
distribution of ConceptBase.cc, then you need to ask the copyright holder for a permission.


\section{Disclaimer}
\label{sec:disclaimer}

All trademarks are owned by their respective owners. This report may contain flaws based on
human errors. We disclaim liability for any such flaws. It may also be that the ConceptBase.cc
does not provide all functionality described in this report, or that the functionality is provided
by other mechanisms as described in this report. Links to external websites are provided for
informational (academic) purposes. We disclaim responsibility for the views expressed on these websites.

We sometimes use the short form ConceptBase. We then always refer to ConceptBase.cc. 










