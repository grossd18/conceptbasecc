STLport
Home <Home.shtml>
News <News.shtml>
Download <Download.shtml>
SVN <SVN.shtml>
Mailing lists <Maillists.shtml>
Packages <Packages.shtml>
Project history <History.shtml>
Developers <Developers.shtml>
License <License.shtml>
FAQ <FAQ.shtml>
Bug report <BugReport.shtml>
Related issues <Related.shtml>
STLport@SF: Summary <http://sourceforge.net/projects/stlport/>

SourceForge.net Logo <http://sourceforge.net>
Valid CSS! <http://jigsaw.w3.org/css-validator/check/referer>
Valid HTML 4.01 Transitional <http://validator.w3.org/check?uri=referer>
namespace license {


    STLport License Agreement

Boris Fomitchev grants Licensee a non-exclusive, non-transferable,
royalty-free license to use STLport and its documentation without fee.

By downloading, using, or copying STLport or any portion thereof,
Licensee agrees to abide by the intellectual property laws and all other
applicable laws of the United States of America, and to all of the terms
and conditions of this Agreement.

Licensee shall maintain the following copyright and permission notices
on STLport sources and its documentation unchanged:

Copyright 1999, 2000 Boris Fomitchev

This material is provided "as is", with absolutely no warranty expressed
or implied. Any use is at your own risk. Permission to use or copy this
software for any purpose is hereby granted without fee, provided the
above notices are retained on all copies. Permission to modify the code
and to distribute modified code is granted, provided the above notices
are retained, and a notice that the code was modified is included with
the above copyright notice.

The Licensee may distribute binaries compiled with STLport (whether
original or modified) without any royalties or restrictions.

The Licensee may distribute original or modified STLport sources,
provided that:

    * The conditions indicated in the above permission notice are met;
    * The following copyright notices are retained when present, and
      conditions provided in accompanying permission notices are met:

      Copyright 1994 Hewlett-Packard Company

      Copyright 1996,97 Silicon Graphics Computer Systems, Inc.

      Copyright 1997 Moscow Center for SPARC Technology.

      / Permission to use, copy, modify, distribute and sell this
      software and its documentation for any purpose is hereby granted
      without fee, provided that the above copyright notice appear in
      all copies and that both that copyright notice and this permission
      notice appear in supporting documentation. Hewlett-Packard Company
      makes no representations about the suitability of this software
      for any purpose. It is provided "as is" without express or implied
      warranty./

      / Permission to use, copy, modify, distribute and sell this
      software and its documentation for any purpose is hereby granted
      without fee, provided that the above copyright notice appear in
      all copies and that both that copyright notice and this permission
      notice appear in supporting documentation. Silicon Graphics makes
      no representations about the suitability of this software for any
      purpose. It is provided "as is" without express or implied warranty./

      / Permission to use, copy, modify, distribute and sell this
      software and its documentation for any purpose is hereby granted
      without fee, provided that the above copyright notice appear in
      all copies and that both that copyright notice and this permission
      notice appear in supporting documentation. Moscow Center for SPARC
      Technology makes no representations about the suitability of this
      software for any purpose. It is provided "as is" without express
      or implied warranty./

