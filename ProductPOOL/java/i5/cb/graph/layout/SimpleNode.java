/*
The ConceptBase.cc Copyright

Copyright 1987-2020 The ConceptBase Team. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice, this list of
      conditions and the following disclaimer.
   2. Redistributions in binary form must reproduce the above copyright notice, this list of
      conditions and the following disclaimer in the documentation and/or other materials
      provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE CONCEPTBASE TEAM ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE CONCEPTBASE TEAM OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those of the authors
and should not be interpreted as representing official policies, either expressed or implied,
of the ConceptBase Team.


The ConceptBase Team is represented by

Manfred Jeusfeld, University of Skovde, 54128 Skovde, Sweden
Matthias Jarke, RWTH Aachen, Informatik 5, Ahornstr. 55, 52056 Aachen, Germany
Christoph Quix, RWTH Aachen, Informatik 5, Ahornstr. 55, 52056 Aachen, Germany


This license is a FreeBSD-style copyright license.
Legal home of the FreeBSD copyright license: http://www.freebsd.org/copyright/freebsd-license.html
*/
/*
 * Created on 2005-2-9
 * 
 * Copyright 2005 by Xiang Li,
 *
 * All rights reserved.
 * 
 */
package i5.cb.graph.layout;

import java.util.Enumeration;

import att.grappa.GrappaPoint;
import att.grappa.Node;

/**
 * this class represent a single node and 
 * it delegates all methods to grappa.Node 
 *
 * @author Li Xiang
 * 
 * @version 1.0
 */
public class SimpleNode implements MyNode {

	public SimpleNode(Node node) {
		this._node = node;
		
	}

	/* 
	 * @see i5.cb.graph.layout.MyNode#getCenterPoint()
	 */
	public GrappaPoint getCenterPoint() {
		return (GrappaPoint)_node.getAttributeValue(LayoutGraph.NODE_POSITION);
	}

	Node _node;

	/* (non-Javadoc)
	 * @see i5.cb.graph.layout.MyNode#getHeight()
	 */
	public double getHeight() {
		return ((Double) _node
		.getAttributeValue(LayoutGraph.NODE_HEIGHT)).doubleValue();
	}


	/* 
	 * @see i5.cb.graph.layout.MyNode#getWidth()
	 */
	public double getWidth() {
		return ((Double) _node
		.getAttributeValue(LayoutGraph.NODE_WIDTH)).doubleValue();
	}


	/* 
	 * @see i5.cb.graph.layout.MyNode#isShown()
	 */
	public boolean isShown() {
		if(!isDummyNode())
			return _node.getAttributeValue(LayoutGraph.IS_NODE_SHOWN).equals(Boolean.FALSE);
		else
			return false;
	}


	/* (non-Javadoc)
	 * @see i5.cb.graph.layout.MyNode#setX(double)
	 */
	public void setX(double x) {
		double y = _node.getCenterPoint().y;
		_node.setAttribute(LayoutGraph.NODE_POSITION, new GrappaPoint(x,
				y));
	}


	/* (non-Javadoc)
	 * @see i5.cb.graph.layout.MyNode#setY(double)
	 */
	public void setY(double y) {
		double x = _node.getCenterPoint().x;
		_node.setAttribute(LayoutGraph.NODE_POSITION, new GrappaPoint(x,
				y));
		
	}
	
	private boolean isDummyNode(){
		return _node.getAttributeValue(LayoutGraph.NODE_DUMMY).equals(Boolean.TRUE);
	}

	/* (non-Javadoc)
	 * @see i5.cb.graph.layout.MyNode#getCenterX()
	 */
	public double getCenterX() {
		// TODO Auto-generated method stub
		return getCenterPoint().x;
	}


	/* (non-Javadoc)
	 * @see i5.cb.graph.layout.MyNode#getCenterY()
	 */
	public double getCenterY() {
		// TODO Auto-generated method stub
		return getCenterPoint().y;
	}


	/* (non-Javadoc)
	 * @see i5.cb.graph.layout.MyNode#setShown(boolean)
	 */
	public void validateShown() {
		// TODO Auto-generated method stub
		_node.setAttribute(LayoutGraph.IS_NODE_SHOWN, Boolean.FALSE);
	}


	/* (non-Javadoc)
	 * @see i5.cb.graph.layout.MyNode#inEdgeElements()
	 */
	public Enumeration inEdgeElements() {
		// TODO Auto-generated method stub
		return _node.inEdgeElements();
	}


	/* (non-Javadoc)
	 * @see i5.cb.graph.layout.MyNode#outEdgeElements()
	 */
	public Enumeration outEdgeElements() {
		// TODO Auto-generated method stub
		return _node.outEdgeElements();
	}


	/* This function is used to make the sepration
	 * of compositenode larger than simple node
	 * @see i5.cb.graph.layout.MyNode#getAdditionalSep()
	 */
	public double getAdditionalSep() {
		// TODO Auto-generated method stub
		return 0;
	}


}
