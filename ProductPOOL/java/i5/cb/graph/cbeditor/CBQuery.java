/*
The ConceptBase.cc Copyright

Copyright 1987-2020 The ConceptBase Team. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice, this list of
      conditions and the following disclaimer.
   2. Redistributions in binary form must reproduce the above copyright notice, this list of
      conditions and the following disclaimer in the documentation and/or other materials
      provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE CONCEPTBASE TEAM ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE CONCEPTBASE TEAM OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those of the authors
and should not be interpreted as representing official policies, either expressed or implied,
of the ConceptBase Team.


The ConceptBase Team is represented by

Manfred Jeusfeld, University of Skovde, 54128 Skovde, Sweden
Matthias Jarke, RWTH Aachen, Informatik 5, Ahornstr. 55, 52056 Aachen, Germany
Christoph Quix, RWTH Aachen, Informatik 5, Ahornstr. 55, 52056 Aachen, Germany


This license is a FreeBSD-style copyright license.
Legal home of the FreeBSD copyright license: http://www.freebsd.org/copyright/freebsd-license.html
*/
package i5.cb.graph.cbeditor;


import i5.cb.CBException;
import i5.cb.api.CBanswer;
import i5.cb.api.ICBclient;
import i5.cb.telos.Transform;
import i5.cb.telos.frame.ObjectName;
import i5.cb.telos.frame.TelosParser;
import i5.cb.telos.object.*;
import i5.cb.api.CButil;

import java.io.IOException;
import java.io.StringReader;
import java.util.*;

import javax.xml.parsers.*;

import org.w3c.dom.*;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/** Is used to retrieve telosObjects from the ConceptBase server. Therefore a cbQuery must contain some kind of question it can {@link i5.cb.graph.cbeditor.CBQuery#ask} to the CB server.
 * This can be either a method that returns an {@link i5.cb.telos.object.ITelosObjectSet} or an Individual (a telosobject) which is a query in the CB server.
 */

public class CBQuery {

    /**
     * the string representing our query. This String is supplied as attribute to the CBClient's ask method
     */
    private String m_sQuery;

    private CBFrame m_cbFrame;

    private String m_sLinkType;
    private String m_sSrcDst;

    private TelosObject m_toSourceObj;

    private TelosObject m_toAttrCat;

    private HashMap m_graphtypes;
    private HashMap m_objectsEdges;
    private String m_sAnswerString;

    private String gpropertyKeyLastSet;


    /** Creates a new CBQuery from a QueryString
     * @param sQueryString String representing the Query
     * @param cbf The cbFrame handling this userobject's connection to a ConceptBase server
     * @param obj The objectType to which edges should be returned
     * @param LinkType The Class in which the edges must be contained
     * @param srcdest specify wheter obj should be source or destination of the edges
     */
    public CBQuery(String sQueryString, CBFrame cbf,TelosObject obj,String sLinkType,String srcdest) throws Exception {

        m_cbFrame=cbf;

        m_sAnswerString="CBGraphEditorResult[" + cbf.getGraphicalPalette() + "/pal,"+obj.toString()+"/obj,"+sLinkType+"/cat,"+srcdest+"/objtype]";

        m_sQuery=sQueryString;
        m_sLinkType=sLinkType;
        m_toSourceObj=obj;
        m_sSrcDst=srcdest;
        m_objectsEdges=new HashMap();
        m_graphtypes=new HashMap();

    }

    /** Creates a new CBQuery from a QueryString, storing also the attribute class of the attributes
     * to be shown. The label of the attribute class is used for derived attributes.
     * @param sQueryString String representing the Query
     * @param cbf The cbFrame handling this userobject's connection to a ConceptBase server
     * @param obj The objectType to which edges should be returned
     * @param LinkType The Class in which the edges must be contained
     * @param srcdest specify wheter obj should be source or destination of the edges
     * @param attrCat the attribute category of the attributes to be shown
     */
    public CBQuery(String sQueryString, CBFrame cbf,TelosObject obj,String sLinkType,String srcdest, TelosObject attrCat) throws Exception {

        m_cbFrame=cbf;

        m_sAnswerString="CBGraphEditorResult[" + cbf.getGraphicalPalette() + "/pal,"+obj.toString()+"/obj,"+sLinkType+"/cat,"+srcdest+"/objtype]";

        m_sQuery=sQueryString;
        m_sLinkType=sLinkType;
        m_toSourceObj=obj;
        m_sSrcDst=srcdest;
        m_objectsEdges=new HashMap();
        m_graphtypes=new HashMap();
        m_toAttrCat=attrCat;

    }

    /** Creates a new Query from a QueryString, this Query is used to retrieve single Objects,
     *  the String should look like "find_object[......."
     * @param sQueryString String representing the Query
     * @param cbf The cbFrame handling this userobject's connection to a ConceptBase server
     */
    public CBQuery(String sQueryString, CBFrame cbf)
    {
      m_sAnswerString="CBGraphEditorResultWithoutEdges[" + cbf.getGraphicalPalette() + "/pal]";
      m_sQuery=sQueryString;
      m_cbFrame=cbf;
      m_sLinkType="NoEdges";
      m_objectsEdges=new HashMap();
      m_graphtypes=new HashMap();
    }

    // new query type for bulk queries, ticket #364
    public CBQuery(String sQueryName, StringArray args, CBFrame cbf)
    {
      StringBuffer argsbuffer = new StringBuffer();
      argsbuffer.append(args.get(0));
      for (int i=1; i<args.size; i++){
          argsbuffer.append(","+args.get(i));
      }
      m_sAnswerString="CBGraphEditorResultWithoutEdges[" + cbf.getGraphicalPalette() + "/pal]";
      if (args.size == 0 )
        m_sQuery= "bulk["+ sQueryName + "]";
      else
        m_sQuery= "bulk["+ sQueryName + "," + argsbuffer.toString() + "]";
// System.out.println("new query: " + m_sQuery);
      m_cbFrame=cbf;
      m_sLinkType="NoEdges";
      m_objectsEdges=new HashMap();
      m_graphtypes=new HashMap();
    }
    

    /** Asks this cbQuery's "question" (which can be answered with an ITelosObjectSet)
     * @return return a set containing the CBUserObjects from the result of this query
     */

    public Collection ask() {
      return ask(true);
    }

    public Collection askSorted() {
      return ask(true);
    }

    public Collection askUnSorted() {
      return ask(false);
    }


    public Collection ask(boolean sorted) {

        String name=null;
        Node Sibling;
        m_objectsEdges=new HashMap();
        LinkedList newUserObjectList=new LinkedList();

        // return empty list if not connected
        if(!m_cbFrame.isConnected())
            return newUserObjectList;

        ITelosObjectSet newObjectsSet=TelosObjectSetFactory.produce();
        Document DOM_Root=executeXMLQuery();
        if(DOM_Root!=null) {
            Element result = DOM_Root.getDocumentElement();
            Sibling = result.getFirstChild();
        }
        else {
            Sibling=null;
        }
        //iterate over all objects returned by Cbase
        while( Sibling != null) {
            //pick all objects
            if(Sibling.getNodeName().equals("object")) {
                gpropertyKeyLastSet = "";  // to prevent overwriting a key that was just set before
                CBGraphTypePropertySet propertySet=new CBGraphTypePropertySet();
                Node possibleNameNodes=Sibling.getFirstChild();
                //iterates over all name candidates
                while(possibleNameNodes != null) {
                    // pick name nodes and read data
                    if(possibleNameNodes.getNodeName().equals("name")) {
                        Text NameNode=(Text) possibleNameNodes.getFirstChild();
                        name=(String) NameNode.getNodeValue();
                        ObjectName onObject=CBUtil.parseObjectName(name);
                        //create new TelosObjects and add them to the ITelosOjectSet
                        try {
                            newObjectsSet.add(Transform.toTelosObject(onObject,m_cbFrame.getObi()));
                        }
                        catch(i5.cb.CBException e) {}
                    }
                    if(possibleNameNodes.getNodeName().equals("graphtype")) {
                        //adding graphtypes to HashSet with name as Key, this is the name of the current object
                        Text NameNode=(Text) possibleNameNodes.getFirstChild();
                        String type=(String) NameNode.getNodeValue();
//System.out.println("graphtype(" + name + ")=" + type);
                        m_graphtypes.put(name,type);
                    }
                    if(possibleNameNodes.getNodeName().equals("edges")) {
                        //Todo save edges
                        this.saveEdgesForObject(possibleNameNodes,name);
                    }

                    // add object-specific graphical properties, ticket #397
                    //System.out.println("getGProperties for "+name);
                    if (possibleNameNodes.getNodeName().equals("gproperty")) {
                      getGProperties(possibleNameNodes,propertySet);
                    }

                    possibleNameNodes=possibleNameNodes.getNextSibling();

                }

                //insert propertySet for object 'name' into CBFrame's getPropertiesOfGraphicalTypes
                setGProperties(name, propertySet);

            }
            Sibling = Sibling.getNextSibling();
        }

        Enumeration en;
        if (sorted) 
           en=newObjectsSet.sortedElements();
        else
           en=newObjectsSet.elements();

        while(en.hasMoreElements()) {
            CBUserObject uo=CBUserObject.getCBUserObject((TelosObject)en.nextElement(),m_cbFrame,m_graphtypes);
            HashSet currentEdges=getEdges(uo.getTelosObject());
            if(!currentEdges.isEmpty()) {
                //set  explicit edges for Object
                uo.setEdges(currentEdges);
            }
            else {
                //there is no explicit edge for this object, so we have to construct an implicit edge
                //build TelosLink according to the LinkType of the Query
                if(m_sLinkType.equals("IsA")){
                    Specialization spec=null;
                    if(m_sSrcDst.equals("src"))
                        spec=TelosObject.getSpecialization(m_toSourceObj,uo.getTelosObject());
                    else
                        spec=TelosObject.getSpecialization(uo.getTelosObject(),m_toSourceObj);
                    spec.setImplicit(true);
                    CBUserObject implEdge=CBUserObject.getCBUserObject(spec,m_cbFrame,m_graphtypes);
                    HashSet tmp=new HashSet();
                    tmp.add(implEdge);
                    uo.setEdges(tmp);
                }
                else if(m_sLinkType.equals("InstanceOf")) {
                    Instantiation inst=null;
                    if(m_sSrcDst.equals("src"))
                        inst=TelosObject.getInstantiation(m_toSourceObj,uo.getTelosObject());
                    else
                        inst=TelosObject.getInstantiation(uo.getTelosObject(),m_toSourceObj);
                    inst.setImplicit(true);
                    CBUserObject implEdge=CBUserObject.getCBUserObject(inst,m_cbFrame,m_graphtypes);
                    HashSet tmp=new HashSet();
                    tmp.add(implEdge);
                    uo.setEdges(tmp);
                }
                else if(m_sLinkType.equals("Attribute") ||
                        m_sLinkType.indexOf('!') != -1) {
                    Attribute attr=null;
                    String sLabel="deduced";
                    if(m_toAttrCat!=null) {
                        sLabel=m_toAttrCat.getLabel();
                    }
                    if(m_sSrcDst.equals("src"))
                        attr=TelosObject.getAttribute( m_toSourceObj,sLabel,uo.getTelosObject());
                    else
                        attr=TelosObject.getAttribute(uo.getTelosObject(),sLabel,m_toSourceObj);
                    attr.setImplicit(true);
                    CBUserObject implEdge=CBUserObject.getCBUserObject(attr,m_cbFrame,m_graphtypes);
                    HashSet tmp=new HashSet();
                    tmp.add(implEdge);
                    uo.setEdges(tmp);
                }
                else if(m_sLinkType.equals("NoEdges")) {
                //NOOP
                }
            }
            newUserObjectList.add(uo);
        }
        return newUserObjectList;

    }


    // read the gproperty entries from an XML text into propertySet
    private void getGProperties(Node possibleNameNodes, CBGraphTypePropertySet propertySet) {
       Node Values=possibleNameNodes.getFirstChild();
       String PropertyName="";
       while (Values!=null){
          // read PropertyName
          if (Values.getNodeName().equals("name")){
            Text PropertyNameNode=(Text) Values.getFirstChild();
            PropertyName=(String) PropertyNameNode.getNodeValue();
          }

          //read PropertyValue
          if (Values.getNodeName().equals("value")){
            CBGraphTypeProperty property;
            Text PropertyValueNode=(Text) Values.getFirstChild();
            String sPropertyValue=(String) PropertyValueNode.getNodeValue();
            //erase \" at beginning and end
            sPropertyValue=CButil.decodeStringIfPossible(sPropertyValue);
            //create new  CBGraphTypePropertyObject and add it to the set
            property=new CBGraphTypeProperty(PropertyName,sPropertyValue);
            // System.out.println(PropertyName+": "+sPropertyValue);
            propertySet.put(property);
          }
          Values=Values.getNextSibling();
       } // while
    }


    //insert propertySet for object 'name' into CBFrame's getPropertiesOfGraphicalTypes
    private void setGProperties(String name, CBGraphTypePropertySet propertySet) {
           // "**" prepended to name to avoid collisions with regular graphtypes
           if (name != null) {
              String sKey = "**"+name;
              if (!propertySet.m_GraphTypeProperties.isEmpty()) {
                 // a property set e to be stored
                 m_cbFrame.getPropertiesOfGraphicalTypes().put(sKey,propertySet);
                 gpropertyKeyLastSet = sKey; // this key shall not be removed in this transaction anymore
                 // System.out.println("Set gproperties for: " + name+"="+propertySet.toString());
              } else if (!sKey.equals(gpropertyKeyLastSet) && m_cbFrame.getPropertiesOfGraphicalTypes().containsKey(sKey)) {
                 // remove old property set because the new one is empty
                 // System.out.println("Remove gproperties for: " + name);
                 m_cbFrame.getPropertiesOfGraphicalTypes().remove(sKey);
              }
            }
    }


    /** Executes the Query with the QueryString given in the constructor
     *  and return the parsed XML answer of CBase as an DOM-Document
     *  @return DOM representation of the CBase answer
     */
    private Document executeXMLQuery() {
        InputSource source;
        StringReader reader;
        CBanswer ans;
        ObjectBaseInterface obi;
        ICBclient cb;
        Document DOM_Tree;
        obi=m_cbFrame.getObi();
        cb=obi.getCBClient();
        //create XML-Parser
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setIgnoringElementContentWhitespace(true);
        factory.setValidating(false);
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            //ask database
            ans= cb.ask(m_sQuery,"OBJNAMES",m_sAnswerString,"Now");
            //create stream for XML Parser InputSource
            if(ans.getResult().equals("nil")) {
                return null;
            }
            reader= new StringReader(ans.getResult());
            source = new InputSource(reader);
            //parse answer and generate DOMTree
            DOM_Tree= builder.parse(source);
            return DOM_Tree;
        }
        catch (ParserConfigurationException pce) {
            // Parser with specified options can't be built
            //pce.printStackTrace();
            return null;
        }
        catch (SAXException sxe) {
            // Error generated during parsing
            Exception x = sxe;
            if (sxe.getException() != null) {
                x = sxe.getException();
            }
            java.util.logging.Logger.getLogger("global").fine(x.getMessage());
            return null;
        }
        catch(CBException cbx) {
            //Error while asking database
            java.util.logging.Logger.getLogger("global").fine(cbx.getMessage());
            return null;
        }
        catch (IOException ioe) {
            // I/O error
            //ioe.printStackTrace();
            return null;
        }
    }

    /* Return the Hashmap containing the graphTypes of all Objects which occured
     * in the answer of CBase, this is empty if ask() wasn?t called before
     * @return graphTypes of Object?s in the CBase answer
     */
    public HashMap getGraphTypes() {
        return m_graphtypes;
    }
    /** Save all edges belonging to the object, in HashMap m_objectsEdges
     * with ObjName as Key
     * @param edges Root of the edges subtree in the XML answer
     * @param ObjName Name of the TelosObject
     */
    private void saveEdgesForObject(Node edges,String ObjName) {
        HashSet objectsEdgesNames=new HashSet();
        String name=null;
        Node Edges = edges.getFirstChild();
        //iterate over all edges returned by Cbase
        while( Edges != null) {
            CBGraphTypePropertySet gpropertySet=new CBGraphTypePropertySet();
            Node Attributes=Edges.getFirstChild();
            while(Attributes!=null) {
                if(Attributes.getNodeName()=="name") {
                    Text NameNode=(Text) Attributes.getFirstChild();
                    name=(String) NameNode.getNodeValue();
                    objectsEdgesNames.add(name);
                }
                if(Attributes.getNodeName()=="graphtype") {
                    //adding graphtypes to HashSet with name as Key, this is the name of the current object
                    Text NameNode=(Text) Attributes.getFirstChild();
                    String type=(String) NameNode.getNodeValue();
                    m_graphtypes.put(name,type);
                }

                // add object-specific graphical properties, ticket #397
                //System.out.println("getGProperties for "+name);
                if (Attributes.getNodeName().equals("gproperty")) {
                  getGProperties(Attributes,gpropertySet);
                }

                Attributes = Attributes.getNextSibling();
            }
            setGProperties(name, gpropertySet);
            Edges = Edges.getNextSibling();
        }
        m_objectsEdges.put(ObjName,objectsEdgesNames);
    }
    /** returns a collection containing the edges associated to the TelosObject
     *@param to Telosobject
     *@return Collection containing the TelosObjects which represent the edges
     *
     */
    public HashSet getEdges(TelosObject to) {
        HashSet hsEdges=new HashSet();
        ObjectBaseInterface obi=m_cbFrame.getObi();
        HashSet edgenames=(HashSet) m_objectsEdges.get(to.toString());
        if(edgenames!=null) {
            Iterator iterator=edgenames.iterator();
            while(iterator.hasNext()) {
                //create new telosobject here
                String name=(String) iterator.next();
                TelosParser parser = new TelosParser(new StringReader(name));
                try {
                    ObjectName objN=parser.objectName();
                    hsEdges.add(CBUserObject.getCBUserObject(Transform.toTelosObject(objN,obi),m_cbFrame,m_graphtypes));
                }
                catch(i5.cb.telos.frame.ParseException e) {
                    java.util.logging.Logger.getLogger("global").fine(e.getMessage());
                }
                catch(i5.cb.CBException e) {
                    java.util.logging.Logger.getLogger("global").fine(e.getMessage());
                }
            }
            return hsEdges;
        }
        return hsEdges;
    }

}//CBQuery

