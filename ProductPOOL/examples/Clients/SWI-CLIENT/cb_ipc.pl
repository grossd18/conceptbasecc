%=============================================*
% File: cb_ipc.pl
% Author: Daniel Gross
% Created: 2020-02-24 (2020-02-25)
% -----------------------------------------
% swi prolog ipc access to cb
% (c) 2020 Daniel Gross 
% grossd18@gmail.com 
%=============================================*

/**
* Copyright 2020 Daniel Gross. All rights reserved.
* 
* Redistribution and use in source and binary forms, with or without modification, are permitted
* provided that the following conditions are met:
* 
*    1. Redistributions of source code must retain the above copyright notice, this list of
*       conditions and the following disclaimer.
*    2. Redistributions in binary form must reproduce the above copyright notice, this list of
*       conditions and the following disclaimer in the documentation and/or other materials
*       provided with the distribution.
* 
* THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
* PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE CONCEPTBASE TEAM OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
* OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
* OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* 
* ----
* 
* 
* This license is a FreeBSD-style copyright license.
* Legal home of the FreeBSD copyright license: http://www.freebsd.org/copyright/freebsd-license.html
*/

:- module(cb_ipc,[
	
	cb_init_ipc/1,
	cb_init_ipc/2,
	cb_init_ipc/3,

	cb_enroll_me/2, 
	cb_cancel_me/1, 

	cb_tell/2,
	cb_untell/2,
	cb_mkdir/2,
	cb_set_module/2,
	cb_get_module_path/1, 
	cb_purge_module/1,

	cb_ask/2,
	cb_ask/3,
	cb_ask/5,
	
	cb_is_ok/1,
	cb_is_error/1,
	cb_result/2,
	
	cb_mon_init_ipc/2,		
	cb_mon_init_ipc/3,		

	cb_mon_enroll_me/3,
	cb_mon_cancel_me/2,

	cb_mon_tell/3,
	cb_mon_untell/3,
	cb_mon_mkdir/3,	
	cb_mon_set_module/3,
	cb_mon_get_module_path/2,
	cb_mon_purge_module/2,
	
	cb_mon_ask/3,
	cb_mon_ask/4,
	cb_mon_ask/5,

	cb_get_fragment/2,
	cb_fragment/3
	
	]).

% ------------------------------------------------------------------------------------	
% retrieve fragment items
% ------------------------------------------------------------------------------------	

cb_fragment(_, (error, _), error).

cb_fragment(name, (ok, 'SMLfragment'(what(Name), _, _, _, _)), Name) :-
	ground(Name).
%	atom_string(Name, Name_str).

cb_fragment(class, (ok, 'SMLfragment'(_, _, in(Class_List), _, _)), Class) :-
	ground(Class_List),
	member(class(Class), Class_List).

cb_fragment(parent, (ok, 'SMLfragment'(_, _, _ ,isa(Class_List), _)), Class) :-
	ground(Class_List),	
	member(class(Class), Class_List).
	
cb_fragment(attribute, (ok, 'SMLfragment'(_, _, _ ,_, with(WithList))), with(AttribCat,property(Label, Target))) :-
	ground(WithList),
	member(attrdecl(CatList, PropList), WithList),
	member(AttribCat, CatList),
	member(property(Label, Target), PropList).
%	atom_string(Target, Target_str).
	
cb_fragment(attr_cat, (ok, 'SMLfragment'(what(Source), _, _ ,_, with(WithList))), attr_cat(Source, Label, CatList)) :-
	ground(Source),
	var(CatList),
	member(attrdecl(CatList, PropList), WithList),
	member(property(Label, _Target_str), PropList).
	
cb_fragment(attr_cat, (ok, 'SMLfragment'(what(Source), _, _ ,_, with(WithList))), attr_cat(Source, Label, CatList)) :-
	ground(Source),
	ground(CatList),
	member(attrdecl(CatList2, PropList), WithList),
	subset(CatList, CatList2),
	member(property(Label, _Target_str), PropList).
	
cb_fragment(attr_cat, (ok, 'SMLfragment'(what(Source), _, _ ,_, with(WithList))), attr_cat(Source, Label, CatList, not(Neg_CatList))) :-
	ground(Source),
	ground(CatList),
	ground(Neg_CatList),
	member(attrdecl(CatList2, PropList), WithList),
	subset(CatList, CatList2),
	member(property(Label, _Target_str), PropList),
	\+ subset(Neg_CatList, CatList2).
	
cb_fragment(property, SMLFragment, property(Label, Target)) :-
	ground(SMLFragment),
	cb_fragment(attribute, SMLFragment, with(_,property(Label, Target))).
	
	
% ------------------------------------------------------------------------------------	

% for obtaining a fragment of an attribute identified by Source!Label
cb_get_fragment((Source, Label), Result) :-
	ground(Source),	
	format(string(S), "get_object[~q!~w/objname,TRUE/dedIn,TRUE/dedWith,TRUE/dedIsa]", [Source, Label]),
	cb_ask(S, "FRAGMENT", Result1),
	cb_is_ok(Result1),
	cb_result(Result1, R),
	read_term_from_atom(R, Fragment_Term,[variable_names(Names)]),
	maplist(call, Names),
	Result = (ok, Fragment_Term).
	
	
cb_get_fragment(Object, Result) :-
	ground(Object),	
	format(string(S), "get_object[~q/objname,TRUE/dedIn,TRUE/dedWith,TRUE/dedIsa]", [Object]),
	cb_ask(S, "FRAGMENT", Result1),
	cb_is_ok(Result1),
	cb_result(Result1, R),
	read_term_from_atom(R, Fragment_Term,[variable_names(Names)]),
	maplist(call, Names),
	Result = (ok, Fragment_Term).

% ------------------------------------------------------------------------------------	

cb_is_ok((ok, _)). % for monad
cb_is_error((error, _)). % for monad

cb_result((_, Result), Result).
% ------------------------------------------------------------------------------------	

aux_result((error, "nil"), (error, [])) :- !.

aux_result((error, "no"), Result) :-
	get_error_messages(Result1),
	Result = (error, Result1), !.

aux_result((error, error), Result) :-
	get_error_messages(Result1),
	Result = (error, Result1), !.
	
aux_result(Result, Result) :- !.

% ------------------------------------------------------------------------------------	

get_error_messages(Msgs) :-
	aux_get_error_messages([], Msgs).
		
aux_get_error_messages(Msgs, New_Msgs) :-
	next_message('ERROR_REPORT', M),
	(M = [] ->
		New_Msgs = Msgs
		;
	aux_get_error_messages( [M | Msgs], New_Msgs)).
	
next_message(Type, M) :-
	cb_client(Server_str, Client_str),
	format(string(Msg), 'ipcmessage(~q, ~q, NEXT_MESSAGE, [~w]).', [Client_str, Server_str, Type]),
	aux_send_message(Msg),
	aux_read_message_result(M).	
	
aux_read_message_result(Result) :-
	connectedReadStream(IStream), 	
	read_line_to_codes(IStream,S1),			% read length, has nl
	atom_string(A, S1),
	atom_number(A, N1),
	N2 is N1 + 1, 							% len is one minus real len
	read_string(IStream, N2, S2),
%	read_line_to_codes(IStream,S12),		% read away last nl	
%	write(S12),
%	read_string(IStream, "\n", "\r", _End1, _Result1),
%	read_string(IStream, ".", "\r", _End, S2),
	read_term_from_atom(S2, X2, []),
	X2 = ipcanswer(_Server_str, _, Message),
	read_term_from_atom(Message, X3, []),
	(X3 = empty_queue ->
		Result = []
		;
	 X3 = ipcmessage(_, _, _, [Result])).
	
	
	
% ------------------------------------------------------------------------------------	

aux_send_message(Msg) :-
	connectedWriteStream(OStream),	
	string_length(Msg, Len),
	B1 is 88,									% letter X in ascii, by convention
	B2 is (Len div 16777216) mod 256,			% encoding of length in 4 bytes
	B3 is (Len div 65536) mod 256,
	B4 is (Len div 256) mod 256,
	B5 is (Len mod 256),
	write(OStream, B1),
	write(OStream, B2),
	write(OStream, B3),
	write(OStream, B4),
	write(OStream, B5),							% no nl for CB, before msg
	write(OStream, Msg),
	nl(OStream),								% only at end of stream
	flush_output(OStream).

% ------------------------------------------------------------------------------------	

aux_read_result(Result) :-
	connectedReadStream(IStream), 
	read_line_to_codes(IStream,_S),			% read first two chars / is ignored
	read_line_to_codes(IStream,S2),			% read result	
	read_term_from_atom(S2, X2, []),
	X2 = ipcanswer(_Server_str, Result0, Client_str),
	Result = (Result0, Client_str).

aux_read_result((error, 'unexpected_error in aux_read_result')).
	
	
% ------------------------------------------------------------------------------------	

aux_query_read_result(Result) :-
%	trace,
	connectedReadStream(IStream), 
	read_line_to_codes(IStream,S1),			% read length, has nl
	atom_string(A, S1),
	atom_number(A, N1),
	N2 is N1 + 1,							% len to read is + 1
	read_string(IStream, N2, S2),
	read_term_from_atom(S2, X2, []),
	X2 = ipcanswer(_Server_str, Result0, Result1),
	Result = (Result0, Result1), !.
	
aux_query_read_result((error, 'unexpected_error in aux_query_read_result')).
	
% ------------------------------------------------------------------------------------	

recov_1(Xs, Y) :-
	Y = (error, Xs), !.

% ------------------------------------------------------------------------------------	

cb_mon_init_ipc(R1, R2) :-
	cb_is_ok(R1),
	cb_init_ipc(4001, R2), !.
	
cb_mon_init_ipc(R, R) :- !.


cb_mon_init_ipc(Host, R1, R2) :-
	cb_is_ok(R1),
	cb_init_ipc(Host, 4001, R2), !.
	
cb_mon_init_ipc(_Host, R, R) :- !.

% ------------------------------------------------------------------------------------	
	
cb_init_ipc(Result) :-
	cb_init_ipc(4001, Result).
	
cb_init_ipc(Port, Result) :-
	retractall(cb_client(_, _)),
	retractall(connectedWriteStream(_)),
	retractall(connectedReadStream(_)),
	set_prolog_flag(allow_variable_name_as_functor, true),
	catch(
		(aux_connect(Port),
		 Result = (ok, connected)),
		Xs,
		recov_1(Xs, Result)), !.
	
cb_init_ipc(_, (error, 'unexpected_error in cb_init_ipc')).

cb_init_ipc(Host, Port, Result) :-
	retractall(cb_client(_, _)),
	retractall(connectedWriteStream(_)),
	retractall(connectedReadStream(_)),
	catch(
		(aux_connect(Host, Port),
		 Result = (ok, connected)),
		Xs,
		recov_1(Xs, Result)), !.
	
cb_init_ipc(_, _, (error, 'unexpected_error in cb_init_ipc')).


aux_connect(Port) :-
	tcp_socket(Socket),
	gethostname(Host), % local host
	tcp_connect(Socket, Host:Port),
	tcp_open_socket(Socket, INs, OUTs),
	assert(connectedReadStream(INs)),
	assert(connectedWriteStream(OUTs)).

aux_connect(Host, Port) :-
	tcp_socket(Socket),
	tcp_connect(Socket, Host:Port),
	tcp_open_socket(Socket, INs, OUTs),
	assert(connectedReadStream(INs)),
	assert(connectedWriteStream(OUTs)).

% ------------------------------------------------------------------------------------	

cb_mon_enroll_me(User, Result_in, Result_out) :-
	cb_is_ok(Result_in) ->
		(cb_enroll_me(User, Result_out)
		;
		Result_out = Result_in).
	
cb_enroll_me(User, Result) :-
	catch(
		(aux_enroll(User),
		 aux_read_enroll_result(Result)),
		Xs,
		recov_1(Xs, Result)), !.

aux_enroll(User) :- 
	format(string(Msg), 'ipcmessage(~q, ~q, ENROLL_ME, [~q, ~q]).', ["", "","swi_ipc", User]),
	aux_send_message(Msg).

aux_read_enroll_result(Result) :-
	connectedReadStream(IStream), 
	read_line_to_codes(IStream,_S),			% read first two chars line / length
	read_line_to_codes(IStream,S2),			% read result of enroll line
	read_term_from_atom(S2, X2, []),
	X2 = ipcanswer(Server_str, Result0, Client_str),
	Result = (ok, Result0),
	assert(cb_client(Server_str, Client_str)), !.

aux_read_enroll_result((error, 'unexpected_error in aux_read_enroll_result')).

% ------------------------------------------------------------------------------------	
cb_mon_cancel_me(Result_in, Result_out) :-
	cb_is_ok(Result_in) ->
		cb_cancel_me(Result_out)
		;
		Result_out = Result_in.
		
cb_cancel_me(Result) :-
	catch(
		(aux_cancel_me,
		 aux_read_result(Result)),
		Xs,
		recov_1(Xs, Result)),
		connectedWriteStream(OStream),		
		connectedReadStream(IStream),		
        close(IStream, [force(true)]),
        close(OStream, [force(true)]),
		retractall(cb_client(_, _)),
		retractall(connectedWriteStream(_)),
		retractall(connectedReadStream(_)), !.

cb_cancel_me((error, 'unexpected_error in cb_cancel_me')).
		
aux_cancel_me :-
	cb_client(Server_str, Client_str),
	format(string(Msg), 'ipcmessage(~q, ~q, CANCEL_ME, []).', [Client_str, Server_str]),
	aux_send_message(Msg).

% ------------------------------------------------------------------------------------	

cb_mon_tell(Frame, Result_in, Result_out) :-
	cb_is_ok(Result_in) ->
		cb_tell(Frame, Result_out)
		;
		Result_out = Result_in.

cb_tell(Frame, Result) :-
	catch(
		aux_tell_untell('TELL', Frame, Result),
		Xs,
		recov_1(Xs, Result)), !.

cb_mon_untell(Frame, Result_in, Result_out) :-
	cb_is_ok(Result_in) ->
		cb_untell(Frame, Result_out)
		;
		Result_out = Result_in.

cb_untell(Frame, Result) :-
	catch(
		aux_tell_untell('UNTELL', Frame, Result),
		Xs,
		recov_1(Xs, Result)), !.

cb_mon_set_module(Frame, Result_in, Result_out) :-
	cb_is_ok(Result_in) ->
		cb_set_module(Frame, Result_out)
		;
		Result_out = Result_in.
						
cb_set_module(Frame, Result) :-
	catch(
		aux_tell_untell('SET_MODULE_CONTEXT', Frame, Result),
		Xs,
		recov_1(Xs, Result)), !.

cb_mon_mkdir(Module, Result_in, Result_out) :-
	cb_is_ok(Result_in) ->
		cb_mkdir(Module, Result_out)
		;
		Result_out = Result_in.

cb_mkdir(Module, Result) :-
	catch(
		(format(string(M), '~q in Module end ', Module),
		aux_tell_untell('TELL', M, Result)),
		Xs,
		recov_1(Xs, Result)), !.

cb_mon_get_module_path(Result_in, Result_out) :-
	cb_is_ok(Result_in) ->
		cb_get_module_path(Result_out)
		;
		Result_out = Result_in.
		
cb_get_module_path(Result) :-
	catch(
		aux_get_module_path(Result),
		Xs,
		recov_1(Xs, Result)), !.
		
aux_get_module_path(Result) :-
	cb_client(Server_str, Client_str),
	format(string(Msg), 'ipcmessage(~q, ~q, GET_MODULE_PATH, []).', [Client_str, Server_str]),
	aux_send_message(Msg),
	aux_read_result(Result0),
	aux_result(Result0, Result).
		
	
aux_tell_untell(X, Frame, Result) :-
	cb_client(Server_str, Client_str),
	format(string(Msg), 'ipcmessage(~q, ~q, ~w, [~q]).', [Client_str, Server_str, X, Frame]),
	aux_send_message(Msg),
	aux_read_result(Result0),
	aux_result(Result0, Result).
	
% ------------------------------------------------------------------------------------	

cb_mon_purge_module(Result_in, Result_out) :-
	cb_is_ok(Result_in) ->
		cb_purge_module(Result_out)
		;
		Result_out = Result_in.

cb_purge_module(Result) :-
	cb_ask('OBJNAMES', "purgeModule", "FRAME", "Now", Result).
	
cb_mon_ask(Query, Result_in, Result_out) :-
	cb_is_ok(Result_in) ->
		cb_ask(Query, Result_out)
		;
		Result_out = Result_in.

cb_ask(Query, Result) :-
	cb_ask('OBJNAMES', Query, "default", "Now", Result).

cb_mon_ask(Query, AnswerRep, Result_in, Result_out) :-
	cb_is_ok(Result_in) ->
		cb_ask(Query, AnswerRep, Result_out)
		;
		Result_out = Result_in.
		
cb_ask(Query, AnswerRep, Result) :-
	cb_ask('OBJNAMES', Query, AnswerRep, "Now", Result).

cb_mon_ask(Query, AnswerRep, Rollbacktime, Result_in, Result_out) :-
	cb_is_ok(Result_in) ->
		cb_ask(Query, AnswerRep, Rollbacktime, Result_out)
		;
		Result_out = Result_in.
			
cb_ask(Format, Query, AnswerRep, Rollbacktime, Result) :-
	catch(
		aux_ask(Format, Query, AnswerRep, Rollbacktime, Result),
		Xs,
		recov_1(Xs, Result)).
				
aux_ask(Format, Query, AnswerRep, Rollbacktime, Result) :-
	cb_client(Server_str, Client_str),
	format(string(Msg), 'ipcmessage(~q, ~q, ASK, [~w, ~q, ~q, ~q]).', [Client_str, Server_str,Format, Query, AnswerRep, Rollbacktime]),
	aux_send_message(Msg),
	aux_query_read_result(Result0),
	aux_result(Result0, Result).
	
	
% -----------------------------------------------
% aux_ask: 			aux_query_read_result, 
%					get_error_messages
%
% cb_cancel_me:	
% aux_tell_untell:	aux_read_result
%					get_error_messages

cb_test_setup :-
	phrase(testdcg_connect, _L1, Result1),
	cb_is_ok(Result1),	
	cb_purge_module(Result2),
	cb_is_ok(Result2), 
	set_prolog_flag(allow_variable_name_as_functor, true), !.
	
	
cb_test_cleanup	:-
	cb_cancel_me(Result),
	cb_is_ok(Result).			
			
testdcg_connect -->
	cb_mon_init_ipc,
%	cb_mon_init_ipc('192.168.1.104'),	
	cb_mon_enroll_me("dsl").

:- begin_tests(tell_test, [setup(cb_test_setup), cleanup(cb_test_cleanup)]).

test(tell_test1, []) :-
	cb_tell("Employee in Class with attribute empno: Integer; name: String end", Result2),
	cb_is_ok(Result2),
	cb_ask("get_object[Employee/objname]", Result3),
	cb_is_ok(Result3),	
	cb_purge_module(Result4),
	cb_is_ok(Result4),
	cb_ask("get_object[Employee/objname]", Result5),
	cb_is_error(Result5).
		
test(tell_test1, []) :-
	cb_get_module_path(Result5),
	cb_is_ok(Result5),
	cb_tell("Employee in Class with attribute empno: Integer; name: String end", Result1),	
	cb_is_ok(Result1).
	
test(tell_test2, []) :-
	cb_tell("Employee in Class with attribute empno: Integer; name: String", Result1),	% end is missing
	\+ cb_is_ok(Result1).
	
	
test(tell_test3, []) :-
	cb_tell("Employee in Class2 with attribute empno: Integer; name: String end", Result1),	% Class2 not defined
	cb_is_error(Result1).
	
test(tell_test4, []) :- % happy cases
	cb_tell("Employee in Class with attribute empno: Integer; name: String end", Result1),
	cb_is_ok(Result1),	
	cb_tell("bill in Employee with empno billsempno: 123 end", Result2),
	cb_is_ok(Result2),
	cb_tell("gwendoline in Employee with empno billsempno: 123 end",Result3),	
	cb_is_ok(Result3),
	cb_ask("get_object[bill/objname]", Result4),
	cb_is_ok(Result4),
	cb_result(Result4, "bill in Employee with \n   empno\n    billsempno : 123\nend \n"),
	cb_tell("UnnamedEmployee in QueryClass isA Employee with constraint c: $ not exists n/String (this name n) $ end", Result5),
	cb_is_ok(Result5),
	cb_ask("UnnamedEmployee", "LABEL", Result6),
	cb_is_ok(Result6),
	cb_result(Result6, "bill,gwendoline").
		
test(tell_test5, []) :-
	cb_tell("Employee in Class with attribute empno: Integer; name: String end", Result1),
	cb_is_ok(Result1),	
	cb_tell("bill in Employee with empno billsempno: 123 end", Result2),
	cb_is_ok(Result2),
	cb_tell("gwendoline in Employee with empno billsempno: 123 end",Result3),	
	cb_is_ok(Result3),
	cb_ask("get_object[bill/objname2]", Result4),		% incorrect param name
	cb_is_error(Result4).
		
test(tell_test6, []) :-
	cb_tell("Employee in Class with attribute empno: Integer; name: String end", Result1),
	cb_is_ok(Result1),	
	cb_tell("bill in Employee with empno billsempno: 123 end", Result2),
	cb_is_ok(Result2),
	cb_tell("gwendoline in Employee with empno billsempno: 123 end",Result3),	
	cb_is_ok(Result3),
	cb_ask("get_object2[bill/objname]", Result4),		% unknown function
	cb_is_error(Result4).
		
test(tell_test7, []) :-
	cb_tell("Employee in Class with attribute empno: Integer; name: String end", Result1),
	cb_is_ok(Result1),	
	cb_tell("bill in Employee with empno billsempno: 123 end", Result2),
	cb_is_ok(Result2),
	cb_tell("gwendoline in Employee with empno billsempno: 123 end",Result3),	
	cb_is_ok(Result3),
	cb_ask("get_object[bill/objname", Result4),		% syntax error
	cb_is_error(Result4).
	
test(tell_test8, []) :- % happy cases
	cb_tell("Employee in Class with attribute empno: Integer; name: String end", Result1),
	cb_is_ok(Result1),	
	cb_tell("bill in Employee with empno billsempno: 123 end", Result2),
	cb_is_ok(Result2),
	cb_tell("gwendoline in Employee with empno billsempno: 123 end",Result3),	
	cb_is_ok(Result3),
	cb_ask("get_object[bill/objname]", Result4),
	cb_is_ok(Result4),
	cb_result(Result4, "bill in Employee with \n   empno\n    billsempno : 123\nend \n"),
	cb_tell("UnnamedEmployee in QueryClass isA Employee with constraint c: $ not exists n/String (this name n) $ end", Result5),
	cb_is_ok(Result5),
	cb_ask("UnnamedEmployee", "LABEL", Result6),
	cb_is_ok(Result6),
	cb_result(Result6, "bill,gwendoline"),
	cb_untell("bill in Employee with empno billsempno: 123 end", Result7),	
	cb_is_ok(Result7),
	cb_ask("UnnamedEmployee", "LABEL", Result8),
	cb_is_ok(Result8),
	cb_result(Result8, "gwendoline").

test(tell_test9, [nondet]) :- 
	cb_tell("Person in Class end", Result1),
	cb_is_ok(Result1),		
	cb_tell("Employee in Class isA Person with attribute empno: Integer; name: String end", Result2),
	cb_is_ok(Result2),		
	cb_get_fragment('Employee', Result3),
	cb_is_ok(Result3),
	cb_fragment(name, Result3, 'Employee'),
	cb_fragment(class, Result3, 'Class'),
	cb_fragment(parent, Result3, 'Person'),
	cb_fragment(attribute,Result3, with(attribute, property(empno, 'Integer'))),
	cb_fragment(attribute,Result3, with(attribute, property(name, 'String'))).
	
		
:- end_tests(tell_test).



