{*
The ConceptBase.cc Copyright

Copyright 1987-2014 The ConceptBase Team. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice, this list of
      conditions and the following disclaimer.
   2. Redistributions in binary form must reproduce the above copyright notice, this list of
      conditions and the following disclaimer in the documentation and/or other materials
      provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE CONCEPTBASE TEAM ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE CONCEPTBASE TEAM OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those of the authors
and should not be interpreted as representing official policies, either expressed or implied,
of the ConceptBase Team.


The ConceptBase Team is represented by

Matthias Jarke, RWTH Aachen, Informatik 5, Ahornstr. 55, 52056 Aachen, Germany
Manfred Jeusfeld, Tilburg University, Warandelaan 2, 5037 AB Tilburg, The Netherlands
Christoph Quix, RWTH Aachen, Informatik 5, Ahornstr. 55, 52056 Aachen, Germany


This license is a FreeBSD-style copyright license.
Legal home of the FreeBSD copyright license: http://www.freebsd.org/copyright/freebsd-license.html
*}
{$set syntax=PlainToronto}
Individual frankfurt in city 
end frankfurt
Individual muenchen in city 
end muenchen
Individual hamburg in city 
end hamburg
Individual duesseldorf in city 
end duesseldorf
Individual bremen in city 
end bremen
Individual wangerooge in city 
end wangerooge
Individual juist in city 
end juist
Individual paris in city 
end paris
Individual london in city 
end london
Individual hof in city 
end hof
Individual bayreuth in city 
end bayreuth
Individual kobenhavn in city 
end kobenhavn
Individual stuttgart in city 
end stuttgart
Individual hannover in city 
end hannover
Individual berlin in city 
end berlin
Individual nuernberg in city 
end nuernberg
Individual sofia in city 
end sofia
Individual helsinki in city 
end helsinki
Individual roma in city 
end roma
Individual glasgow in city 
end glasgow
Individual saarbruecken in city 
end saarbruecken
Individual nice in city 
end nice
Individual milano in city 
end milano
Individual koeln_bonn in city 
end koeln_bonn
Individual oslo in city 
end oslo
Individual bucuresti in city 
end bucuresti
Individual dublin in city 
end dublin
Individual zagreb in city 
end zagreb
Individual manchester in city 
end manchester
Individual amsterdam in city 
end amsterdam
Individual barcelona in city 
end barcelona
Individual moskva in city 
end moskva
Individual wien in city 
end wien
Individual muenster in city 
end muenster
Individual tunis in city 
end tunis
Individual goeteborg in city 
end goeteborg
Individual chicago in city 
end chicago
Individual bruxelles in city 
end bruxelles
Individual casablanca in city 
end casablanca
Individual dallas in city 
end dallas
Individual helgoland in city 
end helgoland
Individual madrid in city 
end madrid
