{*
The ConceptBase.cc Copyright

Copyright 1987-2014 The ConceptBase Team. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice, this list of
      conditions and the following disclaimer.
   2. Redistributions in binary form must reproduce the above copyright notice, this list of
      conditions and the following disclaimer in the documentation and/or other materials
      provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE CONCEPTBASE TEAM ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE CONCEPTBASE TEAM OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those of the authors
and should not be interpreted as representing official policies, either expressed or implied,
of the ConceptBase Team.


The ConceptBase Team is represented by

Matthias Jarke, RWTH Aachen, Informatik 5, Ahornstr. 55, 52056 Aachen, Germany
Manfred Jeusfeld, Tilburg University, Warandelaan 2, 5037 AB Tilburg, The Netherlands
Christoph Quix, RWTH Aachen, Informatik 5, Ahornstr. 55, 52056 Aachen, Germany


This license is a FreeBSD-style copyright license.
Legal home of the FreeBSD copyright license: http://www.freebsd.org/copyright/freebsd-license.html
*}
{$set syntax=PlainToronto}
frankfurt!F1 in city!con_to with
departure
   dept_time: 1140
arrival
   arr_time: 1235
flnr
   flight_number: "lh962"
end frankfurt!F1

frankfurt!F2 in city!con_to with
departure
   dept_time: 2045
arrival
   arr_time: 2150
flnr
   flight_number: "lh808"
end frankfurt!F2

frankfurt!F3 in city!con_to with
departure
   dept_time: 940
arrival
   arr_time: 1040
flnr
   flight_number: "lh370"
end frankfurt!F3

frankfurt!F4 in city!con_to with
departure
   dept_time: 840
arrival
   arr_time: 935
flnr
   flight_number: "lh423"
end frankfurt!F4

frankfurt!F5 in city!con_to with
departure
   dept_time: 1040
arrival
   arr_time: 1135
flnr
   flight_number: "lh312"
end frankfurt!F5

frankfurt!F6 in city!con_to with
departure
   dept_time: 2040
arrival
   arr_time: 2135
flnr
   flight_number: "lh973"
end frankfurt!F6

frankfurt!F7 in city!con_to with
departure
   dept_time: 1740
arrival
   arr_time: 1835
flnr
   flight_number: "lh969"
end frankfurt!F7

muenchen!F8 in city!con_to with
departure
   dept_time: 920
arrival
   arr_time: 1015
flnr
   flight_number: "lh752"
end muenchen!F8

muenchen!F9 in city!con_to with
departure
   dept_time: 1510
arrival
   arr_time: 1615
flnr
   flight_number: "lh359"
end muenchen!F9 

bremen!F10 in city!con_to with
departure
   dept_time: 1000
arrival
   arr_time: 1030
flnr
   flight_number: "du070"
end bremen!F10

wangerooge!F11 in city!con_to with
departure
   dept_time: 1150
arrival
   arr_time: 1220
flnr
   flight_number: "du010"
end wangerooge!F11

juist!F12 in city!con_to with
departure
   dept_time: 1525
arrival
   arr_time: 1620
flnr
   flight_number: "du034"
end juist!F12

bremen!F13 in city!con_to with
departure
   dept_time: 815
arrival
   arr_time: 930
flnr
   flight_number: "lh975"
end bremen!F13

bremen!F14 in city!con_to with
departure
   dept_time: 710
arrival
   arr_time: 805
flnr
   flight_number: "lh714"
end bremen!F14

muenchen!F15 in city!con_to with
departure
   dept_time: 1555
arrival
   arr_time: 1710
flnr
   flight_number: "lh818"
end muenchen!F15

frankfurt!F16 in city!con_to with
departure
   dept_time: 800
arrival
   arr_time: 910
flnr
   flight_number: "af741"
end frankfurt!F16 

frankfurt!F17 in city!con_to with
departure
   dept_time: 1120
arrival
   arr_time: 1150
flnr
   flight_number: "ba725"
end frankfurt!F17

hof!F18 in city!con_to with
departure
   dept_time: 1820
arrival
   arr_time: 1835
flnr
   flight_number: "ns106"
end hof!F18

frankfurt!F19 in city!con_to with
departure
   dept_time: 1630
arrival
   arr_time: 1730
flnr
   flight_number: "ns105"
end frankfurt!F19

bayreuth!F20 in city!con_to with
departure
   dept_time: 1010
arrival
   arr_time: 1025
flnr
   flight_number: "ns101"
end bayreuth!F20

bayreuth!F21 in city!con_to with
departure
   dept_time: 745
arrival
   arr_time: 845
flnr
   flight_number: "ns100"
end bayreuth!F21

kobenhavn!F22 in city!con_to with
departure
   dept_time: 730
arrival
   arr_time: 835
flnr
   flight_number: "sk641"
end kobenhavn!F22

stuttgart!F23 in city!con_to with
departure
   dept_time: 1620
arrival
   arr_time: 1755
flnr
   flight_number: "dw076"
end stuttgart!F23

bremen!F24 in city!con_to with
departure
   dept_time: 2015
arrival
   arr_time: 2035
flnr
   flight_number: "ba769"
end bremen!F24

london!F25 in city!con_to with
departure
   dept_time: 1715
arrival
   arr_time: 1935
flnr
   flight_number: "ba768"
end london!F25

hannover!F26 in city!con_to with
departure
   dept_time: 700
arrival
   arr_time: 755
flnr
   flight_number: "lh720"
end hannover!F26

hannover!F27 in city!con_to with
departure
   dept_time: 1620
arrival
   arr_time: 1700
flnr
   flight_number: "ba774"
end hannover!F27

hannover!F28 in city!con_to with
departure
   dept_time: 1725
arrival
   arr_time: 1830
flnr
   flight_number: "dw097"
end hannover!F28

muenchen!F29 in city!con_to with
departure
   dept_time: 1330
arrival
   arr_time: 1500
flnr
   flight_number: "lh1930"
end muenchen!F29

sofia!F30 in city!con_to with
departure
   dept_time: 1000
arrival
   arr_time: 1110
flnr
   flight_number: "lz127"
end sofia!F30

helsinki!F31 in city!con_to with
departure
   dept_time: 1730
arrival
   arr_time: 1835
flnr
   flight_number: "lh029"
end helsinki!F31

stuttgart!F32 in city!con_to with
departure
   dept_time: 2035
arrival
   arr_time: 2130
flnr
   flight_number: "lh923"
end stuttgart!F32

nuernberg!F33 in city!con_to with
departure
   dept_time: 655
arrival
   arr_time: 845
flnr
   flight_number: "lh056"
end nuernberg!F33

muenchen!F34 in city!con_to with
departure
   dept_time: 1330
arrival
   arr_time: 1505
flnr
   flight_number: "lh1936"
end muenchen!F34

roma!F35 in city!con_to with
departure
   dept_time: 1530
arrival
   arr_time: 1700
flnr
   flight_number: "az476"
end roma!F35

kobenhavn!F36 in city!con_to with
departure
   dept_time: 1715
arrival
   arr_time: 1820
flnr
   flight_number: "sk649"
end kobenhavn!F36

frankfurt!F37 in city!con_to with
departure
   dept_time: 1755
arrival
   arr_time: 2120
flnr
   flight_number: "ba957"
end frankfurt!F37

frankfurt!F38 in city!con_to with
departure
   dept_time: 1610
arrival
   arr_time: 1650
flnr
   flight_number: "dw135"
end frankfurt!F38

frankfurt!F39 in city!con_to with
departure
   dept_time: 1615
arrival
   arr_time: 1745
flnr
   flight_number: "lh150"
end frankfurt!F39

milano!F40 in city!con_to with
departure
   dept_time: 1205
arrival
   arr_time: 1320
flnr
   flight_number: "az454"
end milano!F40

berlin!F41 in city!con_to with
departure
   dept_time: 1130
arrival
   arr_time: 1235
flnr
   flight_number: "ba3005"
end berlin!F41

milano!F42 in city!con_to with
departure
   dept_time: 725
arrival
   arr_time: 840
flnr
   flight_number: "az450"
end milano!F42

muenchen!F43 in city!con_to with
departure
   dept_time: 1120
arrival
   arr_time: 1215
flnr
   flight_number: "lh716"
end muenchen!F43

oslo!F44 in city!con_to with
departure
   dept_time: 1655
arrival
   arr_time: 1840
flnr
   flight_number: "sk623"
end oslo!F44

bucuresti!F45 in city!con_to with
departure
   dept_time: 1515
arrival
   arr_time: 1620
flnr
   flight_number: "lh371"
end bucuresti!F45

muenchen!F46 in city!con_to with
departure
   dept_time: 1115
arrival
   arr_time: 1215
flnr
   flight_number: "lh621"
end muenchen!F46

dublin!F47 in city!con_to with
departure
   dept_time: 920
arrival
   arr_time: 1220
flnr
   flight_number: "ei652"
end dublin!F47

frankfurt!F48 in city!con_to with
departure
   dept_time: 1620
arrival
   arr_time: 1745
flnr
   flight_number: "lh366"
end frankfurt!F48

hannover!F49 in city!con_to with
departure
   dept_time: 1055
arrival
   arr_time: 1140
flnr
   flight_number: "lh722"
end hannover!F49

berlin!F50 in city!con_to with
departure
   dept_time: 1440
arrival
   arr_time: 1630
flnr
   flight_number: "da785"
end berlin!F50

manchester!F51 in city!con_to with
departure
   dept_time: 1030
arrival
   arr_time: 1250
flnr
   flight_number: "lh077"
end manchester!F51

hannover!F52 in city!con_to with
departure
   dept_time: 1915
arrival
   arr_time: 2010
flnr
   flight_number: "lh724"
end hannover!F52 

amsterdam!F53 in city!con_to with
departure
   dept_time: 1835
arrival
   arr_time: 1940
flnr
   flight_number: "lh087"
end amsterdam!F53

milano!F54 in city!con_to with
departure
   dept_time: 850
arrival
   arr_time: 1035
flnr
   flight_number: "lh1353"
end milano!F54

koeln_bonn!F55 in city!con_to with
departure
   dept_time: 1005
arrival
   arr_time: 1105
flnr
   flight_number: "ba3004"
end koeln_bonn!F55

helsinki!F56 in city!con_to with
departure
   dept_time: 955
arrival
   arr_time: 1110
flnr
   flight_number: "ay825"
end helsinki!F56

berlin!F57 in city!con_to with
departure
   dept_time: 1805
arrival
   arr_time: 1910
flnr
   flight_number: "pa655"
end berlin!F57

frankfurt!F58 in city!con_to with
departure
   dept_time: 900
arrival
   arr_time: 1000
flnr
   flight_number: "pa634"
end frankfurt!F58

hannover!F59 in city!con_to with
departure
   dept_time: 1920
arrival
   arr_time: 2105
flnr
   flight_number: "lh1984"
end hannover!F59

barcelona!F60 in city!con_to with
departure
   dept_time: 1250
arrival
   arr_time: 1450
flnr
   flight_number: "lh171"
end barcelona!F60

frankfurt!F61 in city!con_to with
departure
   dept_time: 1230
arrival
   arr_time: 1735
flnr
   flight_number: "su256"
end frankfurt!F61

nice!F62 in city!con_to with
departure
   dept_time: 1650
arrival
   arr_time: 1805
flnr
   flight_number: "af1734"
end nice!F62

nice!F63 in city!con_to with
departure
   dept_time: 1650
arrival
   arr_time: 1800
flnr
   flight_number: "af1734"
end nice!F63

paris!F64 in city!con_to with
departure
   dept_time: 1820
arrival
   arr_time: 1935
flnr
   flight_number: "af746"
end paris!F64

wien!F65 in city!con_to with
departure
   dept_time: 1845
arrival
   arr_time: 2010
flnr
   flight_number: "lh1381"
end wien!F65

muenchen!F66 in city!con_to with
departure
   dept_time: 1100
arrival
   arr_time: 1215
flnr
   flight_number: "pa684"
end muenchen!F66

berlin!F67 in city!con_to with
departure
   dept_time: 2030
arrival
   arr_time: 2130
flnr
   flight_number: "ba3135"
end berlin!F67

berlin!F68 in city!con_to with
departure
   dept_time: 700
arrival
   arr_time: 815
flnr
   flight_number: "ba3161"
end berlin!F68

berlin!F69 in city!con_to with
departure
   dept_time: 1200
arrival
   arr_time: 1315
flnr
   flight_number: "ba3163"
end berlin!F69

amsterdam!F70 in city!con_to with
departure
   dept_time: 720
arrival
   arr_time: 825
flnr
   flight_number: "lh081"
end amsterdam!F70

hamburg!F71 in city!con_to with
departure
   dept_time: 815
arrival
   arr_time: 905
flnr
   flight_number: "lh276"
end hamburg!F71

roma!F72 in city!con_to with
departure
   dept_time: 1800
arrival
   arr_time: 2115
flnr
   flight_number: "lh303"
end roma!F72

kobenhavn!F73 in city!con_to with
departure
   dept_time: 1800
arrival
   arr_time: 1915
flnr
   flight_number: "sk627"
end kobenhavn!F73

wien!F74 in city!con_to with
departure
   dept_time: 1655
arrival
   arr_time: 1815
flnr
   flight_number: "os405"
end wien!F74

tunis!F75 in city!con_to with
departure
   dept_time: 855
arrival
   arr_time: 1145
flnr
   flight_number: "tu748"
end tunis!F75

frankfurt!F76 in city!con_to with
departure
   dept_time: 900
arrival
   arr_time: 1155
flnr
   flight_number: "sk630"
end frankfurt!F76

hamburg!F77 in city!con_to with
departure
   dept_time: 915
arrival
   arr_time: 1005
flnr
   flight_number: "lh188"
end hamburg!F77

chicago!F78 in city!con_to with
departure
   dept_time: 1715
arrival
   arr_time: 830
flnr
   flight_number: "lh431"
end chicago!F78

berlin!F79 in city!con_to with
departure
   dept_time: 715
arrival
   arr_time: 815
flnr
   flight_number: "ba3115"
end berlin!F79

muenchen!F80 in city!con_to with
departure
   dept_time: 1050
arrival
   arr_time: 1145
flnr
   flight_number: "lh430"
end muenchen!F80

bruxelles!F81 in city!con_to with
departure
   dept_time: 1820
arrival
   arr_time: 1950
flnr
   flight_number: "sn987"
end bruxelles!F81

casablanca!F82 in city!con_to with
departure
   dept_time: 950
arrival
   arr_time: 1545
flnr
   flight_number: "lh383"
end casablanca!F82

casablanca!F83 in city!con_to with
departure
   dept_time: 950
arrival
   arr_time: 1545
flnr
   flight_number: "lh381"
end casablanca!F83

hamburg!F84 in city!con_to with
departure
   dept_time: 1430
arrival
   arr_time: 1720
flnr
   flight_number: "ay854"
end hamburg!F84

nuernberg!F85 in city!con_to with
departure
   dept_time: 1100
arrival
   arr_time: 1140
flnr
   flight_number: "lh404"
end nuernberg!F85

nuernberg!F86 in city!con_to with
departure
   dept_time: 630
arrival
   arr_time: 725
flnr
   flight_number: "lh927"
end nuernberg!F86

duesseldorf!F87 in city!con_to with
departure
   dept_time: 720
arrival
   arr_time: 810
flnr
   flight_number: "lh728"
end duesseldorf!F87

bremen!F88 in city!con_to with
departure
   dept_time: 810
arrival
   arr_time: 855
flnr
   flight_number: "ba3036"
end bremen!F88

dallas!F89 in city!con_to with
departure
   dept_time: 1705
arrival
   arr_time: 1125
flnr
   flight_number: "lh439"
end dallas!F89

frankfurt!F90 in city!con_to with
departure
   dept_time: 1010
arrival
   arr_time: 1105
flnr
   flight_number: "lh334"
end frankfurt!F90

frankfurt!F91 in city!con_to with
departure
   dept_time: 925
arrival
   arr_time: 1010
flnr
   flight_number: "dw131"
end frankfurt!F91

helgoland!F92 in city!con_to with
departure
   dept_time: 1130
arrival
   arr_time: 1215
flnr
   flight_number: "hx024"
end helgoland!F92

saarbruecken!F93 in city!con_to with
departure
   dept_time: 1035
arrival
   arr_time: 1125
flnr
   flight_number: "dw132"
end saarbruecken!F93

frankfurt!F94 in city!con_to with
departure
   dept_time: 1140
arrival
   arr_time: 1235
flnr
   flight_number: "lh962"
end frankfurt!F94

madrid!F95 in city!con_to with
departure
   dept_time: 1640
arrival
   arr_time: 1915
flnr
   flight_number: "ib516"
end madrid!F95

berlin!F96 in city!con_to with
departure
   dept_time: 705
arrival
   arr_time: 810
flnr
   flight_number: "pa635"
end berlin!F96 

paris!F97 in city!con_to with
departure
   dept_time: 1130
arrival
   arr_time: 1325
flnr
   flight_number: "lh1325"
end paris!F97

hof!F98 in city!con_to with
departure
   dept_time: 1100
arrival
   arr_time: 1235
flnr
   flight_number: "ns104"
end hof!F98

frankfurt!F99 in city!con_to with
departure
   dept_time: 1255
arrival
   arr_time: 1525
flnr
   flight_number: "ib511"
end frankfurt!F99

duesseldorf!F100 in city!con_to with
departure
   dept_time: 1000
arrival
   arr_time: 1055
flnr
   flight_number: "lh277"
end duesseldorf!F100
