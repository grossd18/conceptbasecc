{*
The ConceptBase.cc Copyright

Copyright 1987-2014 The ConceptBase Team. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice, this list of
      conditions and the following disclaimer.
   2. Redistributions in binary form must reproduce the above copyright notice, this list of
      conditions and the following disclaimer in the documentation and/or other materials
      provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE CONCEPTBASE TEAM ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE CONCEPTBASE TEAM OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those of the authors
and should not be interpreted as representing official policies, either expressed or implied,
of the ConceptBase Team.


The ConceptBase Team is represented by

Matthias Jarke, RWTH Aachen, Informatik 5, Ahornstr. 55, 52056 Aachen, Germany
Manfred Jeusfeld, Tilburg University, Warandelaan 2, 5037 AB Tilburg, The Netherlands
Christoph Quix, RWTH Aachen, Informatik 5, Ahornstr. 55, 52056 Aachen, Germany


This license is a FreeBSD-style copyright license.
Legal home of the FreeBSD copyright license: http://www.freebsd.org/copyright/freebsd-license.html
*}
{$set syntax=PlainToronto}


D_ProjNummer in Daten
end D_ProjeNummer

D_MaName in Daten
end D_MaName

D_PlName in Daten
end D_PlName

D_ProjAbrMonat in Daten
end D_ProjAbrMonat

D_ProjAbrGB in Daten
end D_ProjAbrGB

D_ProjAbrUntMa in Daten
end D_ProjAbrUntMa

D_ProjAbrUntPl in Daten
end D_ProjAbrUntPl

D_ProjAbrSpesen in Daten
end D_ProjAbrSpesen

D_ProjAbrSpesenSum in Daten
end D_ProjAbrSpesenSum

D_ProjAbrGesStdSum in Daten
end D_ProjAbrGesStdSum

D_ProjAbrSpesVorOrt in Daten
end D_ProjAbrSpesVorOrt

D_ProjAbrStundenSum in Daten
end D_ProjAbrStundenSum

D_ProjAbrLeistung in Daten
end D_ProjAbrLeistung

D_ProjAbrBonusExtra in Daten
end D_ProjAbrBonusExtra

D_ProjAbrStundenSum in Daten
end D_ProjAbrStundenSum

D_ProjAbrUntKd in Daten
end D_ProjAbrUntKd

D_ProjAbrSachlUnt in Daten
end D_ProjAbrSachlUnt

D_ProjAbrRechnUnt in Daten
end D_ProjAbrRechnUnt

D_MonIntLeistung in Daten
end D_MonIntLeistung

D_MonIntSpesen in Daten
end D_MonIntSpesen

D_MonIntSpesenSum in Daten
end D_MonIntSpesenSum

D_MonIntStundenSum in Daten
end D_MonIntStundenSum

D_MonIntUntMa in Daten
end D_MonIntUntMa

D_MonIntUntPv in Daten
end D_MonIntUntPv

D_MonIntSachlUnt in Daten
end D_MonIntSachlUnt

D_MonIntRechnUnt in Daten
end D_MonIntRechnUnt

D_MonZusSpesen in Daten
end D_MonZusSpesen

D_MonZusUrlaub in Daten
end D_MonZusUrlaub

D_MonZusSchule in Daten
end D_MonZusSchule

D_MonZusSachlUnt in Daten
end D_MonZusSachlUnt

D_MonZusRechnUnt in Daten
end D_MonZusRechnUnt

D_SpesenBetrag in Daten
end D_SpesenBetrag

D_SpesBelSachlUnt in Daten
end D_SpesBelSachlUnt

D_SpesBelRechnUnt in Daten
end D_SpesBelRechnUnt

D_SpesenBetragNetto in Daten
end D_SpesenBetragNetto

D_SpesenBetragSum in Daten
end D_SpesenBetragSum

D_SonstBetrag in Daten
end D_SonstBetrag

D_MaUrlaub in Daten
end D_MaUrlaub

D_KostenSatz in Daten
end D_KostenSatz

D_ProjStatFestpreis in Daten
end D_ProjStatFestpreis

D_ProjStatOperativ in Daten
end D_ProjStatOperativ

D_ProjStatRechng in Daten
end D_ProjStatRechng

D_ProjStatFertig in Daten
end D_ProjStatFertig

D_ProjStatFertigSum in Daten
end D_ProjStatFertigSum

D_ProjStatUnt in Daten
end D_ProjStatUnt

D_HalbFertigSumme in Daten
end D_HalbFertigSumme

D_HalbFertigRestaufwand in Daten
end D_HalbFertigRestaufwand

D_RechngNummer in Daten
end D_RechngNummer

D_ProjAbrSumme in Daten
end D_ProjAbrSumme

D_RechngUnt in Daten
end D_RechngUnt

D_Bezahlt in Daten
end D_Bezahlt

D_KundenName in Daten
end D_KundenName

D_SollProjektOk in Daten
end D_SollProjektOk

D_KalkStunden in Daten
end D_KalkStunden

D_AngebotsNummer in Daten
end D_AngebotsNummer

D_AngebotsLeistung in Daten
end D_AngebotsLiestung

D_AngebotUntGL in Daten
end D_AngebotUntGL

D_AngebotsOk in Daten
end D_AngebotsOk

D_AuftragUnt in Daten
end D_AuftragUnt

D_ProjSollStunden in Daten
end D_ProjSollStunden

D_ProjAuftPlUnt in Daten
end D_ProjAuftPlUnt

D_PerbitUrlaub in Daten
end D_PerbitUrlaub

D_PerbitSchulung in Daten
end D_PerbitSchulung

D_KorrekturInfo in Daten
end D_KorrekturInfo

