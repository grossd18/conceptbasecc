The copyright for the sources of the deductive object base ConceptBase 
is with The ConceptBase Team, being the group of people who have contributed to 
the source code. All rights are reserved by the ConceptBase Team.

Code from external parties is included based on appropriate licenses, see
directory doc/ExternalLicenses.

The ConceptBase Team is represented by

   Matthias Jarke (RWTH Aachen)
   Manfred Jeusfeld (Tilburg University) 
   Christoph Quix (RWTH Aachen)

Major contributors to the source code of the system include

   Lutz Bauer (module system)
   Rainer Gallersdörfer (object store)
   Manfred Jeusfeld (CB server, logic foundation, function component) 
   Eva Krüger (integrity component)
   Thomas List (object store)
   Hans Nissen (module system) 
   Christoph Quix (CB server, view component)
   Christoph Radig (object store)
   René Soiron (optimizer)
   Martin Staudt (CB server, query component)
   Kai von Thadden (query component)
   Hua Wang (answer formatting)

Additional contributions to the source code came among others from

   Masoud Asady
   Markus Baumeister
   Ulrich Bonn
   Stefan Eherer  
   Michael Gebhardt (user interface)
   Dagmar Genenger
   Michael Gocek
   Rainer Hermanns
   David Kensche
   André Klemann
   Rainer Langohr
   Tobias Latzke
   Xiang Li
   Yong Li 
   Farshad Lashgari (active rules)
   Andreas Miethsam
   Martin Pöschmann
   Achim Schlosser
   Tobias Schöneberg
   Claudia Welter
   Thomas Wenig

ConceptBase is an implementation of the Telos knowledge representation language
designed at the University of Toronto and partner universities. We thank
the designers of Telos, in particular

   Alex Borgida
   Sol Greenspan
   John Mylopoulos
   Manolis Koubarakis
   

Tilburg, 2009-07-31


