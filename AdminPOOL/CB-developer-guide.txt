                 Guide for prospective ConceptBase developers
                            Manfred Jeusfeld 
                        7-Aug-2009 (4-Feb-2019)


So, you downloaded the sources of ConceptBase. You even compiled it from
its sources following the steps of CB-developer-config.txt.
And now you want to play a bit with the code, possibly adding some
function, or removing a bug. Wou are welcome to do so.
This little text is meant as an incomplete guide to the ConceptBase
sources.


1. The programming languages and architecture
=============================================

Well, we use Java (mostly for the ConceptBase clients), C/C++ (for the object store),
and Prolog (mostly for the ConceptBase server engine). That is quite a 
heterogeneous collection. It corresponds roughly to the architecture of ConceptBase:

   User interface (CBiva) 
       ||ipcmessage||
   ConceptBase server engine (CBserver)
       ||P-tuple   ||
   Object store (libCos3)


The interface between CBiva and CBserver is realized as an inter-process communication (TCP/IP),
accepting a data structure called ipcmessage. That is a container for relatively high-level
commands to update the CBserver database and querying it. You might compare that to SQL.

The object store (libCos3: i.e. revision 3 of the C-based object store) is linked to the CBserver
process. Hence, the CBserver calls the library functions of libCos3. The main data structure is
a P-tuple P(id,x,l,y) that os also the basis for the O-Telos language realized by ConceptBase.



2. User interface CBiva
=======================

The sources are located in java/i5/cb. The are two main components of CBiva: the workbench 
and the graph editor. You find the sources in the respective subdirectories. I cannot say 
a lot about them. They just heavily rely on Java Swing.

Another Java tool is the CBshell. It is a command line client to the CBserver allowing to
write scripts to automate certain tasks.

There is a small Java program PrologPreProcessor.java that has nothing to do
with clients. It is just a tools to translate the Prolog sources to certain Prolog dialects
such as SWI-Prolog. As we only support SWI-Prolog, this pre-processor has lost a bit its
justification to exist. However, it will allow us to support other Prolog systems in the future.




2a. Java-based API
==================

If you want to link your application to a ConceptBase server, then we recommend using the
Java API. It is documented at
   http://conceptbase.sourceforge.net/userManual80/cbm007.html#sec111
Example source code for Java-based clients are in your ConceptBase installation directory, see
subdirectory
   examples/Clients/JavaClient
The Java API is string-based and easy to use. Rather than rasing exceptions, it returns appropriate
answers indicating what went wrong.




3. ConceptBase server engine CBserver
=====================================

The sources are located in serverSources/Prolog_Files. There are quite a lot of modules
and they are to a large degree messy and not well-factored. Still, there is a bit of a structure
there.

The modules BIM2C.pro and ExternalCodeLoader.pro interface to libCos3.

The module PropositionProcessor.pro provides an abstract interface to P-tuples, basically offering
them like a predicate. 

The module PropositionsToFragment.pro converts P-tuples (also referred to as 'propositions') to
an internal data structure called fragments, being syntax trees of O-Telos frames. The module
FragmentToPropositions.pro does the inverse.

The module Literals.pro is the central module of the engine. It evaluates all predicates defined
in rules, constraints, and queries. This module has received most updates in the development
history of ConceptBase. So, here is a central place where new functionality is added to the language.

The modules BDM*.pro are responsible for compiling and managing integrity constraints and 
also one aspect of deductive rules (namely, that they can feed facts into integrity constraints).
The acronym BDM is derived from the authors of some papers on deductive integrity checking
(Bry, Decker, Manthey). Some algorithms of these authors were used.
Note that rules/constraints can be added/removed at any time. So, the compiler for them
is incremental by nature.

The module SMLaxioms.pro realizes the core builtin rules and constraints of ConceptBase.
In particular, there axioms define proper attribution, instantiation, and specialization
on terms of the central P-tuple data structure (a.k.a. propositions). The module
SemanticIntegrity.pro is just a management layer on top of these axioms.

The modules LT*.pro and Rule*.pro realize the second half of deductive rules, in particular their compilation
to Prolog code. The acronym refers to Lloyd & Topor who proposed some of the algorithms used here.

The modules MSFOL*.pro offer functions to compile rules and constraints, mostly to
compile their textual representation into an internal syntax tree. The syntax itself
is defined in parseAss.dcg, a definite clause grammar that is automatically transformed
to Prolog code by the program dcg.pro. As you may guess, the module tokens.dcg
defines the tokenizer (syntax of object labels, values, etc.). 

The syntax of O-Telos frames used to be defined in sml_gramm.dcg. However, this dcg
grammar is now obsolete since the frame parser is now realized by a lex/yacc-based parser
defines in libCos3. The Prolog interface to this new parser is LanguageInterface.pro.

The modules Meta*.pro take care for the management of so-called meta formulas. They are
a distinguishing feature of ConceptBase and we are proud of them.

The module SemanticOptimizer.pro provides some rules to code generated for rules and
constraints. It basically removes redundant predicates.
Related to this are the modules QO*.pro, a cost-based optimizer for rules and constraints.
Recently, the module QO_preproc.pro has been extended quite considerably. It 'repairs'
certain cost-based optimizations to ensure that mandatory orderings of certain predicates
are preserved. For example, you cannot test x<y unless both are bound to values.

The modules ECA*.pro realize the active rule component of ConceptBase. Quite complex stuff :-(

The module CBserverInterface.pro does exactly what the name indicates. It offers the services
that can be called by ConceptBase clients. The main data structures are ipcmessage and
ipcanswer. 



4. ConceptBase object store libCos3
===================================

The object store libCos3 provides a dedicated highly indexed data structure for P-tuples and
some base predicates derived from the P-tuple.

Below is a script for adding a new base predicate, i.e. a predicate that has direct support
from libCos3.

Direct access to object store data
----------------------------------

"It would be nice to directly compute all attributes of a given object x, such as Attr_s(x,id)
should return objects id that are attributes originating from x, independently of the
attribute category. The object store anyway redundantly stores this data."

The script shows how to realize new access predicates on top of the C-based object store (libCos3).
Here are the main steps. We use the Attr_s predicate as an example.

1. Define in BIM2C.pro the new literal prove_C_Attr_s(x,y) using Attr_s_query
from ExternalCodeLoader.pro.

2. Export in ExternalCodeLoader the predicate Attr_s_query/2. For SWI-Prolog, the EXPORT clause
is sufficient. For BIM-Prolog (legacy), you need to list the function c_Attr_s_query for
extern_load and define Attr_s_query using the extern_predicate clause. So in total, three lines to add.

3. In swiCos.c, you need to to define a function swi_Attr_s_query using the envelope operations
for SWI-Prolog (succeed/fail). It calls c_Attr_s_query from objstore.c (objstore.h).

4. In bim2c.c, you need to define some access function c_Attr_s_find, c_Attr_s_getquery,
c_Attr_s_getid. Note that the access functions uses an index for each literal (=predicate).
The Attr_s literal happens to have the index number 7. It is defined as the position of
the new literal in the LITERAL_ID list (trans.cc).

5. In trans.cc (trans_c.h), you need to add "Attr_s" to the Literal_ID list. Note the the first entry
has position number 0. In Literal_getquery, you need to add a new case for Attr_s calling attrSQUERY.
Also add a new case for the print statements after (nr == -1).

6. In Literals.h, you need to increase LITANZ (one more). LITANZ means 'number of literals. Also add
new label Attr_s to the enum Literals clause. I am not sure whether it should be at position 7,
but I added it there anyway. Also define the signature for Attr_s_Literal.

7. In Literals.cc, you need to define the function Attr_s_Literal. This is the main part of the work.

8. In TDB.cc, you need to add a case for Attr_s 

The example Attr_s is a binary predicate. Predicates with more arguments are treated trans.cc by
procedures like Literal4_getquery instead Literal_getquery. An example is the Adot predicate.








