# ConceptBaseCC

Software development for the ConceptBase.cc metamodeling system

---
ConceptBase.cc is a multi-user deductive database system with an object-
oriented (data, class, metaclass, meta-metaclass, etc.) makes it a powerful
tool for metamodeling and engineering of customized modeling languages. The
system is accompanied by a highly configurable graphical user interface that
builds upon the logic-based features of the ConceptBase.cc server. 

Development started in 1987 at the University of Passau, then moved on to
RWTH Aachen (both Germany), and now to University of Skövde (Sweden).

If you want to use this repository, then just clone it and check the file 
   AdminPOOL/CB-developer-config.txt
for instructions. For development, you need a Linux environment,
preferably Ubuntu 16.04. The configuration is a bit cumbersome because
ConceptBase uses a lot of different compilers. You will need in particular
SWI-Prolog 6.6.6 (not later!).

You may also download the sources from
  https://sourceforge.net/projects/conceptbase/files/Sources/
  (File CBPOOL.zip)


Manfred Jeusfeld, 2019-05-26 (2020-01-09)

